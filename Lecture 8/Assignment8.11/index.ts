import express, { Request, Response } from 'express'
import { unkownEndpoint, validate } from './middleware'

const server = express()
server.use(express.json())

const books: Array<Book> = [
    {
        "id": 0,
        "name": "Lord of the rings: Fellowship of the ring",
        "author": "J.R.R Tolkien",
        "read": false
    },
    {
        "id": 1,
        "name": "Lord of the rings: Two towers",
        "author": "J.R.R Tolkien",
        "read": false
    }
]

interface Book {
    id: number
    name: string
    author: string
    read: boolean
}

//Returns a list of all the books
server.get('/api/v1/books/', (req: Request, res: Response) => {
    const query = req.query
    console.log(query.author)
    console.log(books)
    res.send({ books })
})

//Returns a book with a corresponding ID.
server.get("/api/v1/books/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    console.log("Hello from get id: ", id)
    const book = books.find(item => item.id === id)
    if (book === undefined) {
        return res.status(404).send("Error 404, invalid book id")
    }
    res.send(book)
})

//  Creates a new book.
server.post('/api/v1/books/', validate, (req: Request, res: Response) => {
    const body: Book = req.body
    books.push(body)
    res.sendStatus(201)
})

// Modifies an existing book
server.put('/api/v1/books/:id', validate, (req: Request, res: Response) => {
    const body: Book = req.body
    const id = Number(req.params.id)
    const book = books.find(item => item.id === id)
    if (book === undefined) {
        return res.status(404).send("Error 404, invalid book id")
    }else{
        book.name = body.name
        book.author = body.author
        book.read = body.read
    }    
    res.sendStatus(200)
})

// Removes a book with a corresponding id
server.delete('/api/v1/books/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const book = books.find(item => item.id === id)
    console.log(id, book)
    if (book === undefined) {
        return res.status(404).send("Error 404, invalid book id")
    }else{
        //delete books[id] //leaves empty object
        books.splice(books[id].id, 1)
    }
    res.sendStatus(200)    
})







server.use(unkownEndpoint)
const port = 3001
server.listen(port, () => {
    console.log('Server listening port', port)
})