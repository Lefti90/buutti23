import { Request, Response, NextFunction } from 'express'

const logger = (req: Request, res: Response, next: NextFunction) => {
    console.log("Hello from middleware")
    next()
}

const validate = (req: Request, res: Response, next: NextFunction ) => {
    const { id, name, author } = req.body
    if (typeof(id) !== 'number' || typeof(name) !== 'string' || typeof(author) !== 'string') {
        return res.status(400).send('Missing or invalid parameters')
    }
    next()
}

const unkownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send("Error 404")
}

export default logger
export { unkownEndpoint, validate }