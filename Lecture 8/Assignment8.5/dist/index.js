"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = (0, express_1.default)();
let counter = 0;
server.get('/', (_req, res) => {
    res.send("Hello world!");
});
const port = 3000;
server.listen(port);
console.log("Listening port", port);
