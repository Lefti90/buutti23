import express, { Request, Response, NextFunction } from 'express'

const server = express()

const students: Array<string> = []

const middleware = (req: Request, res: Response, next: NextFunction) => {
    console.log("Logger middleware: ")
    const dateTime = new Date()
    console.log(dateTime)
    console.log(req.method)
    console.log(req.url)
    console.log("_______")
    next()
}

server.use(middleware)

server.get('/students', (_req: Request, res: Response) => {
    //students.push("pekka")
    console.log(students)
    res.send({ students })
})



const port = 3000
server.listen(port, () => {
    console.log('Server listening port', port)
})