import express, { Request, Response } from 'express'

const server = express()

let counter = 0

let counters: People = {
    "Aaron": 1,
    "Cecil": 2
}

interface People{
    name: string
    counter: number
}

server.get('/counter/:name', (req: Request, res: Response) => {
    const name = req.params.name as string
    if (counters[name] === undefined) {
        counters[name] = 0
      }
    //console.log(name)
    //if(String(counters[name]))
    // const number = Number(req.query.number)
    // counter = Number.isNaN(number) ? counter + 1 : number
    res.send({ name })
})

const port = 3000
server.listen(port, () => {
    console.log('Server listening port', port)
})