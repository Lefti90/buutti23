import express, { Request, Response } from 'express'

const server = express()

let counter = 0

server.get('/counter', (req: Request, res: Response) => {
    counter++
    res.send(JSON.stringify(counter))

    console.log(req.query)
    console.log(req.query.number)
    let newNumber = Number(req.query.number as string)
    console.log(newNumber)
    // if(newNumber != NaN){
    //     console.log("req.query")
    // }
})

const port = 3000

server.listen(port)
console.log("Listening port", port)