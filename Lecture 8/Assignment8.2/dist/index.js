"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = (0, express_1.default)();
server.get('/', (_req, res) => {
    res.send("Hello world!");
});
server.get('/endpoint2', (_req, res) => {
    res.send("Hello world!2");
});
const port = 3001;
server.listen(port);
console.log("Listening port", port);
