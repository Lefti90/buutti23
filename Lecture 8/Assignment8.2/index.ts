import express, { Request, Response } from 'express'

const server = express()

server.get('/', (_req: Request, res: Response) => {
    res.send("Hello world!")
})


server.get('/endpoint2', (_req: Request, res: Response) => {
    res.send("Hello world!2")
})

const port = 3000

server.listen(port)
console.log("Listening port", port)