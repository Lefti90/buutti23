import { Request, Response, NextFunction } from 'express'

const logger = (req: Request, res: Response, next: NextFunction) => {
    console.log("Logger middleware: ")
    const dateTime = new Date()
    console.log(dateTime)
    console.log(req.method)
    console.log(req.url)
    console.log("_______")
    next()
}

const unkownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send("Error 404")
}

export default logger
export { unkownEndpoint }