import express, { Request, Response, NextFunction } from 'express'
import logger, { unkownEndpoint } from './middleware'

const server = express()

const students: Array<string> = []

server.use(logger)

server.get('/students', (_req: Request, res: Response) => {
    //students.push("pekka")
    console.log(students)
    res.send({ students })
})


server.use(unkownEndpoint)
const port = 3000
server.listen(port, () => {
    console.log('Server listening port', port)
})