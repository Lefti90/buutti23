import express, { Request, Response, NextFunction } from 'express'
import logger, { unkownEndpoint, validate } from './middleware'

const server = express()
server.use(express.json())

const students: Array<Body> = [
    {
        "id": 0,
        "name": "Pekka",
        "score": 9
    },
    {
        "id": 1,
        "name": "Kaisa",
        "score": 8
    }
]

interface Body {
    id: number
    name: string
    score: number
}

server.use(logger)

server.post('/students', validate, (req: Request, res: Response) => {
    const body: Body = req.body
    //push to array
    students.push(body)
    res.sendStatus(201)
})

server.get("/students/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    console.log("Hello from get id: ", id)
    const student = students.find(item => item.id === id)
    if (student === undefined) {
        return res.status(404).send("Error 404, invalid student id")
    }
    res.send(student)
})


let aboveAvg: Array<Body> = []
let highScore: number = 0
let topStudent: Body
server.get('/students', (req: Request, res: Response) => {
    const query = req.query
    console.log(query.score)
    console.log(students)

    let studentScores: number = 0
    let studentAvg: number = 0

    //calculate average scores
    const _averageList = students.map(student => {
        studentScores += student.score
    })
    studentAvg = studentScores/students.length

    //get highest
    const _highesScoringStudent = students.map(student => {
        if(student.score >= highScore){
            highScore = student.score
            topStudent = student
        }
    })

    //get top students
    const _aboveAvg = students.map(student => {
        if(student.score >= studentAvg){
            aboveAvg.push(student)
        }
    })

    //Do all the calculations and return what we asked in query
    //Otherwise return all students
    if(query.score === "avg"){
        //Return average        
        res.send({ studentAvg })
    }else if(query.score === "top"){
        //Return highest scoring students
        res.send({ topStudent })
    }else if(query.score === "aboveAvg"){
        //Return % of students that had score higher or equal to average
        res.send({ aboveAvg })
    }else{
        //Return all students
        res.send({ students })
    }
})







server.use(unkownEndpoint)
const port = 3001
server.listen(port, () => {
    console.log('Server listening port', port)
})