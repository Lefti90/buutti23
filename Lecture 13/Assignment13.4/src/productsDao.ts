import { executeQuery } from './db'

export interface Product {
    id?: number
    name: string
    price: number
}

export const getProducts = async () => {
    const query = 'SELECT * FROM products'
    const result = await executeQuery(query)
    const products: Array<Product> = result.rows
    return products
}

export const getProduct = async (id: number) => {
    const query = 'SELECT * FROM products WHERE id=$1'
    const parameters = [id]
    const result = await executeQuery(query, parameters)
    const product: Product = result.rows[0]
    return product
}

export const addProduct = async (name: string, price: number) => {
    const query = 'INSERT INTO products (name, price) VALUES ($1, $2) RETURNING id'
    const parameters = [name, price]
    const result = await executeQuery(query, parameters)
    const id = result.rows[0].id
    const product: Product = { id, name, price }
    return product
}

export const setProduct = async (id: number, name: string, price: number) => {
    const query = 'UPDATE products SET name=$1, price=$2 WHERE id=$3'
    const parameters = [name, price, id]
    await executeQuery(query, parameters)
    const product: Product = { id, name, price }
    return product
}

export const delProduct = async (id: number) => {
    const query = 'DELETE FROM products WHERE id=$1'
    const parameters = [id]
    await executeQuery(query, parameters)
}