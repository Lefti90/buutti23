import {pool} from "../src/db"
import { jest } from '@jest/globals'
import productsRouter from "../src/productsRouter"
import { getProducts, getProduct, addProduct, delProduct, setProduct } from '../src/productsDao'
import exp from "constants"
import { response } from "express"


describe("Dummy test", () => {
    test('Returns true', () => {
      expect(true).toBe(true)
    })

})

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /products', () => {
    const mockResponse = {
        rows: [
            { id: 101, name: 'Test Item 1', price: 100 },
            { id: 102, name: 'Test Item 2', price: 200 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    //Dummy
    test('Returns true', () => {
        expect(true).toBe(true)
      })

    //Get all products
    test('Get all products', async () => {
        const products = await getProducts()
        console.log(products)
    expect(products).toBe(mockResponse.rows)
    })

    //Get all products (done right)
    test('Get all products', async () => {
        const response = await request(server).get('/products')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})


