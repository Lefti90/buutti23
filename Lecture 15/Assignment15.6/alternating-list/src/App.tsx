function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]
  let counter = 0
  return (
    <>
      <div>
        {
          namelist.map(item => {
            if(counter % 2 === 0){
              counter++
              return <div><b>{item}</b></div>
            } else{
              counter++
              return <div><i>{item}</i></div>
            }
          })
        }
      </div>
    </>
  )
}

export default App
