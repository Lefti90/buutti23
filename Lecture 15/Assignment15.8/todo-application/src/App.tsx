import { ChangeEvent, useState } from "react"
import "./App.css"

interface TodoItemData {
    content: string
    isDone: boolean
}

function App() {
    const [text, setText] = useState("")
    const [items, setItems] = useState<TodoItemData[]>([])
    const [isTrue, setIsTrue] = useState(false)

    const onTextChange = (event: ChangeEvent<HTMLInputElement>) => {
        setText(event.target.value)
    }

    const addToItems = () => {
        const newItem: TodoItemData = { content: text, isDone: false }
        setItems(prevItems => [...prevItems, newItem])
        console.log(items)
    }

    const toggleBool = () => {
        setIsTrue(!isTrue)
    }
    // TODO: Kesken: Vaihtaa kaikkien nappuloiden tilaa
    const ToggleButton = () => {
        return (
            <button onClick={toggleBool}>{isTrue ? "true" : "false"}</button>
        )
    }

    const deleteItem = (index: number) => {
        setItems(prevItems => {
            const newItems = [...prevItems]
            newItems.splice(index, 1)
            return newItems
        })
    }

    const DeleteButton = ({ itemIndex }: { itemIndex: number }) => {
        const handleDelete = () => {
            deleteItem(itemIndex)
        }

        return (
            <button onClick={handleDelete}>Delete</button>
        )
    }

    const TodoItems = () => {
        return (
            <div>
                {items.map((item, index) => {
                    return (
                        <div key={index}>
                            {item.content} <ToggleButton /> <DeleteButton itemIndex={index} />
                        </div>
                    )
                })}
            </div>
        )
    }

    return (
        <>
            <input value={text} onChange={onTextChange} />
            <button onClick={addToItems}>Submit</button>
            <TodoItems />
        </>
    )
}

export default App
