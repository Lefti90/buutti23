import { ChangeEvent, useState } from 'react'

function App() {
  const [text, setText] = useState("")
  const [heading, setHeading] = useState("")

  const onTextChange = (event: ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value)
  }

  const setNewHeading = () =>{
    setHeading(text)
  }

  return (
    <div>
      <h1>Your string is: {heading}</h1>
      <input value={text} onChange={onTextChange} />
      <button onClick={setNewHeading}>Submit</button>
    </div>
  )
}

export default App
