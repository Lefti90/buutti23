interface Instruments {
  name: string
  image: string
  price: number
}

export const Instrument = (props: Instruments) => {
  return (
    <div>
      <h2>{props.name}</h2>
      <img src={props.image}></img>
      <p>{props.price}€</p>
    </div>
  )
}