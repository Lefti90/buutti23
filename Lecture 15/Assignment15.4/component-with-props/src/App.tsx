import { Instrument } from "./Instrument"
import guitarImg from "./guitar.png"
import tubaImg from "./tuba.png"
import violinImg from "./violin.png"
import "./App.css"

function App() {
  return (
    <>
      <div>
        <h1>Top selling instruments</h1>
        <Instrument name="Guitar" image={guitarImg} price={199}/>
        <Instrument name="Tuba" image={tubaImg} price={299}/>
        <Instrument name="Violin" image={violinImg} price={399}/>
      </div>
    </>
  )
}

export default App
