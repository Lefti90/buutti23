import { useState } from 'react'
import "./App.css"

function App() {
  const [playerTurn, setPlayerTurn] = useState("X")
  const [gridStates, setGridStates] = useState(["_", "_", "_", "_", "_", "_", "_", "_", "_"])
  const [gameState, setGamestate] = useState(true)

  const changeGridState = (gridIndex: number) => {
    if(gameState){
      setGridStates(prevGridStates => {
        const newGridStates = [...prevGridStates]
        newGridStates[gridIndex] = playerTurn
        return newGridStates
      })
  
      // Toggle player turn
      setPlayerTurn(prevPlayerTurn => (prevPlayerTurn === "X" ? "O" : "X"))
    }
  }

  const CheckGameState = () => {
    const result = gridStates.filter(grid => grid.includes("_"))
    if(result.length == 0){
      setGamestate(false)
      setPlayerTurn("Game over")
    }

    if(gameState){
      return <div>Game is on</div>
    }else{
      return <div>Game is over</div>
    }
  }

  const refreshPage = () => {
    window.location.reload();
  }

  return (
    <>
      <div>
        <h1>Player turn: {playerTurn}</h1>
        <table>
          <tbody>
            <tr>
              <td><button onClick={() => changeGridState(0)}>{gridStates[0]}</button></td>
              <td><button onClick={() => changeGridState(1)}>{gridStates[1]}</button></td>
              <td><button onClick={() => changeGridState(2)}>{gridStates[2]}</button></td>
            </tr>
            <tr>
              <td><button onClick={() => changeGridState(3)}>{gridStates[3]}</button></td>
              <td><button onClick={() => changeGridState(4)}>{gridStates[4]}</button></td>
              <td><button onClick={() => changeGridState(5)}>{gridStates[5]}</button></td>
            </tr>
            <tr>
              <td><button onClick={() => changeGridState(6)}>{gridStates[6]}</button></td>
              <td><button onClick={() => changeGridState(7)}>{gridStates[7]}</button></td>
              <td><button onClick={() => changeGridState(8)}>{gridStates[8]}</button></td>
            </tr>
          </tbody>
        </table> 
        <CheckGameState/>
        <button onClick={refreshPage}>Reset</button>
      </div>
    </>
  )
}

export default App
