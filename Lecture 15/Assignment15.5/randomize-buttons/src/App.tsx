import { useState } from 'react'
import "./App.css"

function App() {
  const [number1, setNumber1] = useState(0)
  const [number2, setNumber2] = useState(0)
  const [number3, setNumber3] = useState(0)
  const [sum, setSum] = useState(0)

  // const calculateSum = () => {
  //   setSum(number1 + number2 + number3)
  // }

  const increaseNumber1 = () => {
    const newValue = number1 + 1
    setNumber1(newValue)
    setSum(newValue + number2 + number3)
  }

  const increaseNumber2 = () => {
    const newValue = number2 + 1
    setNumber2(newValue)
    setSum(number1 + newValue + number3)
  }

  const increaseNumber3 = () => {
    const newValue = number3 + 1
    setNumber3(newValue)
    setSum(number1 + number2 + newValue)
  }

  // const increaseNumbers = (props) => {
  //   console.log(props.button.value)
  //   if("x" == "number1"){
  //     const newValue = number1 + 1
  //     setNumber1(newValue)
  //     setSum(newValue + number2 + number3)
  //   }else if("x" == "number2"){
  //     const newValue = number1 + 1
  //     setNumber1(newValue)
  //     setSum(number1 + newValue + number3)
  //   }else if("x" == "number3"){
  //     const newValue = number1 + 1
  //     setNumber1(newValue)
  //     setSum(number1 + number2 + newValue)
  //   }else{
  //     setSum(0)
  //   }
  // }  

  return (
    <>
      <div>
        <div><button onClick={increaseNumber1}>{number1}</button></div>
        <div><button onClick={increaseNumber2}>{number2}</button></div>
        <div><button onClick={increaseNumber3}>{number3}</button></div>
        <div>{sum}</div>
      </div>
    </>
  )
}

export default App
