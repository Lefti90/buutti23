// Write a program that prints the sum of integers from 1 to n, with a given number n. For example, if n = 5, program prints 15.

// Solve this task with both a for loop ...
let n = 5
let sum = 0
for (let i = 0; i <= n; i++) {
    sum = sum + i    
}
console.log("FOR: ", sum)

//and a while loop.
let x = 1
let sum2 = 0
while(x <= n){
    sum2 = sum2 + x
    x++
}
console.log("WHILE: ", sum2)