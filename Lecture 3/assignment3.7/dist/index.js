"use strict";
let n = 5;
let sum = 0;
for (let i = 0; i <= n; i++) {
    sum = sum + i;
}
console.log("FOR: ", sum);
let x = 1;
let sum2 = 0;
while (x <= n) {
    sum2 = sum2 + x;
    x++;
}
console.log("WHILE: ", sum2);
