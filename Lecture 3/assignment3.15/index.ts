// Create a program that takes in 3 names and outputs only initial letters of 
// those name separated with dot.

// example: node .\initialLetters.js Jack Jake Mike -> j.j.m

const a = process.argv[2]
const b = process.argv[3]
const c = process.argv[4]

console.log("3.15 a)")
console.log(`${a[0]}.${b[0]}.${c[0]}`)


// Create a program that takes in 3 names, and compares the length of those names. 
// Print out the names ordered so that the longest name is first.

// example: node .\lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe


console.log("3.15 b)")

const names = [a, b, c]
const sortedNames = names.sort(function(x, y){return y.length - x.length})
// ^ tässä käyttäsin names.sort((x,y) => y.length - x.length)
console.log(sortedNames[0], sortedNames[1], sortedNames[2])



