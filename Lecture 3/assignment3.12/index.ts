// From the command line read in three numbers, number1, number2 and number3. 
// Decide their values freely.

// Find the
// a) largest one
// b) smallest one
// c) if they all are equal, print that out

// console.log() its name and value.

const a = process.argv[2]
const b = process.argv[3]
const c = process.argv[4]

if(a === b && b === c){
	console.log("C): They are all equal")
}else{
	const numbers = [["a", a], ["b", b], ["c", c]]

	let highestNum = a
	let highestName = "a"
	let lowestNum = a
	let lowestName = "a"

	numbers.forEach(element => {
		if(element[1] > highestNum){
			highestNum = element[1]
			highestName = element[0]
		}

		if(element[1] < lowestNum){
			lowestNum = element[1]
			lowestName = element[0]
		}
	})

	console.log("A) Highest number: ", highestName, highestNum)
	console.log("B) Lowest number : ", lowestName, lowestNum)
}

