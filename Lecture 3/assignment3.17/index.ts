// Create a program that takes in a string and drops off the last word of any given string, 
// and console.logs it out.

// example: node .\annoyingSubstring.js "Hey I'm alive!" -> Hey I'm

const a = process.argv[2]
let curString = a
let curChar = "a"

do{
	curChar = curString[curString.length-1]
	curString = curString.substring(0, curString.length-1)
}while(curChar != " ")

console.log(curString)

// Mitä tapahtuu jos ajan tämän?
// node annoyingSubstring.js MOIKKA