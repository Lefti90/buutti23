// Create a ATM program to check your balance. Create variables balance, isActive, 
// checkBalance. Write conditional statement that implements the flowchart below.

//Randomizer for testing
const balance = Math.floor(Math.random() * 3)
const isActive = Math.random() < 0.5
const checkBalance = Math.random() < 0.5

console.log("Check your balance?")
if(!checkBalance){
	console.log("No")
	console.log("Have a nice day!")
}else{
	console.log("Yes")
	if(isActive && balance > 0){
		console.log("Account balance: ", balance)
	}else if(!isActive){
		console.log("Your account is not active")
	}else if(isActive && balance == 0){
		console.log("Your account is empty")
	}else{
		console.log("Your balance is negative")
	}
}

