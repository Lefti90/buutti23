// Using the for loop for each problem, print out the following number sequences:

// 0 100 200 300 400 500 600 700 800 900 1000
console.log("T1.")
for (let i = 0; i <= 1000; i = i + 100) {
    console.log(i)    
}
// 1 2 4 8 16 32 64 128
console.log("T2.")
for (let i = 1; i <= 128; i = i + i) {
    console.log(i)    
}
// 3 6 9 12 15
console.log("T3.")
for (let i = 3; i <= 15; i = i + 3) {
    console.log(i)    
}
// 9 8 7 6 5 4 3 2 1 0
console.log("T4.")
for (let i = 9; i >= 0; i--) {
    console.log(i)    
}
// 1 1 1 2 2 2 3 3 3 4 4 4
console.log("T5.")
for (let i = 1; i <= 4; i++) {
    for (let x = 0; x <= 3; x++) {
        console.log(i)
    }
}
// 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
console.log("T6.")
for (let i = 0; i <= 3; i++) {
    for (let x = 0; x <= 4; x++) {
        console.log(x)        
    }    
}