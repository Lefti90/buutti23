// Create a program that takes in a number from commandline that represents month of the year.
//  Use console.log to show how many days there are in the given month number.

const a = process.argv[2]

if (a === "1" || a === "3" || a === "5" || a === "7" || a === "8" || a === "10" || a === "12"){
	console.log("31")
}else if(a === "2"){
	console.log("28 or 29")
}else if(a === "4" || a === "6" || a === "9" || a === "11"){
	console.log("30")
}else{
	console.log("Unkown amount of days")
}