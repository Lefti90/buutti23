// Create a program that takes in one argument from command line, 
// a language code (e.g. "fi", "es", "en"). Console.log "Hello World" for 
// the given language for atleast three languages. It should default to console.log "Hello World".

// Remember to test that the program outputs the right answer in all cases.

const lang = process.argv[2]

if(lang === "fi"){
	console.log("Hei Maailma!")
}else if(lang === "swe"){
	console.log("Hejsan Världen!")
}else if(lang === "es"){
	console.log("Hola Mundo!")
}else{
	console.log("Hello World!")
}