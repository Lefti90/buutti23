"use strict";
// Write a program that prints the sum of integers from 1 to n, with a given number n. For example, if n = 5, program prints 15.
// Solve this task with both a for loop ...
let n = 17;
let sum = 0;
for (let i = 0; i <= n; i++) {
    if (i % 5 == 0 || i % 3 == 0) {
        console.log(i);
        sum = sum + i;
    }
}
console.log("FOR: ", sum);
