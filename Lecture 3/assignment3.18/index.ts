// Create a program that takes in a string, and replaces every occurrence of your given character 
// with your other given character.

// example: node .\replacecharacters.js g h "I have great grades for my grading" 
// -> I have hreat hrades for my hrading

const input = process.argv[2]
const charToChange = process.argv[3]
const newChar = process.argv[4]

let newString = input
let ns = ""

for (let i = 0; i < newString.length; i++) {    
	if(newString[i] == charToChange){
		ns = newString.replace(charToChange, newChar)
		newString = ns
	}
}
console.log(newString)

// Kannattaa myös tutustua metodiin .replaceAll