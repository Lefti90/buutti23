// Create a program that takes in two numbers a and b from the command line.
// Print out "a is greater" if a is bigger than b, and vice versa, and 
// "they are equal" if they are equal
// Modify program to take in a third string argument c, and print out 
// "yay, you guessed the password", if a and b are equal AND c is "hello world"
// Remember to test that the program outputs the right answer in all cases.

const a = process.argv[2]
const b = process.argv[3]

if(a > b){
	console.log("a is bigger than b")
}else if(a < b){
	console.log("b is bigger than a")
}else{
	console.log("they are equal")
}

// Where is the third argument?