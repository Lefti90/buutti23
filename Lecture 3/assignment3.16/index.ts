// Create a program that takes in a string, and modifies the every letter of 
// that string to upper case or lower case, depending on the input

// example: node .\modifycase.js lower "Do you LIKE Snowmen?" -> do you like snowmen

// example: node .\modifycase.js upper "Do you LIKE Snowmen?" -> DO YOU LIKE SNOWMEN

// NOTE remember to take in the 2nd parameter with quotation marks

const a = process.argv[2]

if(a == a.toUpperCase()){
	//a is uppercase change to lower
	console.log(a.toLowerCase())
}else{
	//a is lowercase change to upper
	console.log(a.toUpperCase())
}

// Tämä ei nyt toimi kuten pitäisi. Tehtävässä pitää ottaa kaksi parametria, ensimmäinen on komentosana (upper/lower) ja toinen on teksti, jota muutetaan. Nyt tässä otetaan vain yksi parametri.