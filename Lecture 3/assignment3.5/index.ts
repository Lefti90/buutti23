const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"]

console.log(arr[2], arr[4])

arr.sort()
arr.push("sipuli")
console.log(arr)

//EXTRA
// Remove the first item in the array, and print out again. HINT: shift()
arr.shift()
// Print out every item in this array using .forEach()
arr.forEach(element => {
    console.log(element)
})
// Print out every item that contains the letter ‘r’. HINT: includes()
arr.forEach(element => {
    if(element.includes("r")){
        console.log("Found 'r' in : ", element)
    }        
})
