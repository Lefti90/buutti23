import express from "express"

const server = express()

const languages = ['Javascript','Python','Go','Java','Kotlin','Scala','C','Swift','TypeScript','Ruby']

server.get('/languages', (_req, res) => {
    res.send(languages)
})

server.listen(3000, () => {
    console.log('Server listening port 3000')
})