/* eslint-disable @typescript-eslint/no-explicit-any */
import pg from "pg"
import 'dotenv/config'

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, PG_SSL } = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: PG_SSL === "true"
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = "dbError"
        throw error
    } finally {
        client.release()
    }
}

export const getBlogs = async () => {
    const query = 'SELECT * FROM blogs ORDER BY id DESC;'
    const result = await executeQuery(query)
    return result.rows
}

export const postNewBlog = async (title: string, content: string) => {
    const query = `INSERT INTO blogs(title, content) 
    VALUES ('${title}', '${content}');`
    await executeQuery(query)
    const query1 = 'SELECT * FROM blogs ORDER BY id DESC;'
    const result = await executeQuery(query1)
    return result.rows
}