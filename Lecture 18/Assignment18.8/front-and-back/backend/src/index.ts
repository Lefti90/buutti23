import express from "express"
import { getBlogs, postNewBlog } from "./db"

const server = express()
server.use(express.json())
server.use('/', express.static('./dist/client'))

server.get('/blogs', async (_req, res) => {
    const result = await getBlogs()
    res.send(result)
})

server.post("/blogs/new", async (req, res) => {
    const title = String(req.body.title)
    const content = String(req.body.content)

    if(title === undefined || title === "" || content === undefined || content === ""){
        res.sendStatus(400)
    }else{
        const result = await postNewBlog(title, content)
        res.send(result)
    }
})

server.get('*', (_req, res) => {
    res.sendFile('index.html', { root: './dist/client' })
})

const PORT = 3000
server.listen(PORT, () => {
    console.log(`Listening port ${PORT}`)
})