import { useEffect, useState } from "react"
import "./App.css"

interface Blog {
  id: number
  title: string
  content: string
}

function BlogElement(){
  const [blogs, setBlogs] = useState<Array<Blog>>([])
  useEffect(() => {
    initialize()
  }, [])

  const initialize = async () => {
    const result = await fetch("/blogs")
    const blogRes = await result.json()
    setBlogs(blogRes)
  }

  return(
    <div>{blogs.map(blog => (
      <div key={blog.id}>
        <h2>{blog.title}</h2>
        <p>{blog.content}</p>
      </div>
    ))}</div>
  )
}

function NewBlogElement(){
  const [title, setTitle] = useState("")
  const [content, setContent] = useState("")

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  function handleTitle(event: any){
    setTitle(event.target.value)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  function handleContent(event: any){
    setContent(event.target.value)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleSubmit = async (event: any) =>{
    event.preventDefault()
    const formData = {"title": title, "content": content}
    console.log(title)
    console.log(content)
    try {
      const response = await fetch('/blogs/new', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      })
  
      if (!response.ok) {
        throw new Error('Failed to add blog')
      }
  
      console.log('Blog added successfully')
      
      setTitle("")
      setContent("")
      window.location.reload()
    } catch (error) {
      console.error("Error adding blog")
    }
  }

  return(
    <div>
      <hr></hr>
      <h3>New blog:</h3>
      <form>
      <label>
      Title: 
      <input type="text" name="title" onChange={handleTitle} />
      </label>
      <br></br>
      <label>
      Content: 
      <input type="text" name="content" onChange={handleContent}/>
      </label>
      <br></br>
        <input type="submit" value="Submit" onClick={handleSubmit}/>
      </form>
    </div>
  )
}

function App() {
  return (
    <>
    <div>
      <h1>!MY SICK BLOGSITE!</h1>
      <BlogElement />
      <NewBlogElement />
    </div>
    </>
  )
}

export default App
