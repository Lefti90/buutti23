import express from "express"
import { getLanguages } from "./db"

const server = express()

server.use('/', express.static('./dist/client'))
// const languages = ['Javascript','Python','Go','Java','Kotlin','Scala','C','Swift','TypeScript','Ruby']

server.get('/languages', async (_req, res) => {
    const result = await getLanguages()
    res.send(result)
})

server.get('*', (_req, res) => {
    res.sendFile('index.html', { root: './dist/client' })
})

server.listen(3000, () => {
    console.log('Server listening port 3000')
})