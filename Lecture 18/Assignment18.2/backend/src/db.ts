/* eslint-disable @typescript-eslint/no-explicit-any */
import pg from "pg"
import 'dotenv/config'

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, DEV_STATUS } = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: DEV_STATUS === "production"
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = "dbError"
        throw error
    } finally {
        client.release()
    }
}

export const getLanguages = async () => {
    const query = 'SELECT * FROM languages'
    const result = await executeQuery(query)
    return result.rows
}