import express from "express"
import { getLanguages } from "./db"

const server = express()

// const languages = ['Javascript','Python','Go','Java','Kotlin','Scala','C','Swift','TypeScript','Ruby']

server.get('/languages', async (_req, res) => {
    const result = await getLanguages()
    res.send(result)
})

server.listen(3000, () => {
    console.log('Server listening port 3000')
})