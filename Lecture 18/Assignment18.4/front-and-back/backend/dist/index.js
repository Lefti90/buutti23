"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const db_1 = require("./db");
const server = (0, express_1.default)();
server.use('/', express_1.default.static('./dist/client'));
// const languages = ['Javascript','Python','Go','Java','Kotlin','Scala','C','Swift','TypeScript','Ruby']
server.get('/languages', (_req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield (0, db_1.getLanguages)();
    res.send(result);
}));
server.get('*', (_req, res) => {
    res.sendFile('index.html', { root: './dist/client' });
});
server.listen(3000, () => {
    console.log('Server listening port 3000');
});
