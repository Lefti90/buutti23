"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLanguages = exports.executeQuery = exports.pool = void 0;
/* eslint-disable @typescript-eslint/no-explicit-any */
const pg_1 = __importDefault(require("pg"));
require("dotenv/config");
const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE, DEV_STATUS } = process.env;
exports.pool = new pg_1.default.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: DEV_STATUS === "production"
});
const executeQuery = (query, parameters) => __awaiter(void 0, void 0, void 0, function* () {
    const client = yield exports.pool.connect();
    try {
        const result = yield client.query(query, parameters);
        return result;
    }
    catch (error) {
        console.error(error.stack);
        error.name = "dbError";
        throw error;
    }
    finally {
        client.release();
    }
});
exports.executeQuery = executeQuery;
const getLanguages = () => __awaiter(void 0, void 0, void 0, function* () {
    const query = 'SELECT * FROM languages';
    const result = yield (0, exports.executeQuery)(query);
    return result.rows;
});
exports.getLanguages = getLanguages;
