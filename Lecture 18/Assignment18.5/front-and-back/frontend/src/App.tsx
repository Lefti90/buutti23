import { useEffect, useState } from "react"

interface Language{
  language: string
}

function App() {
  const [languages, setLanguages] = useState<Array<Language>>([])
  useEffect(() => {
    initialize()
  }, [])

  const initialize = async () => {
    const result = await fetch("/languages")
    const languagesRes = await result.json()
    setLanguages(languagesRes)
  }

  return (
    <>
    <div className="App">
     {languages.map((language, i) => <li key={i}>{language.language}</li>)}
     </div>
    </>
  )
}

export default App
