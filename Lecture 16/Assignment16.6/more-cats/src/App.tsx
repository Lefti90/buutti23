import { ChangeEvent, useState } from "react"
import "./App.css"

interface CatPictureProps {
  catFinal: string
  catSeed: number
}

function CatPicture({catFinal, catSeed}: CatPictureProps) {
  const seed = catSeed
  return ( 
    <div>
      <img key={seed} src={catFinal}></img>
    </div>  
  )
}

function App() {
  const [text, setText] = useState("")
  const [catUrl, setCatUrl] = useState("")
  const [catFinal, setCatFinal] = useState("https://cataas.com/cat")
  const [catSeed, setCatSeed] = useState(1) //hacky refresh for component

function handleClick(){
  const newSeed = Math.random()
  if(text === ""){
    setCatSeed(newSeed)
    setCatFinal("https://cataas.com/cat")
  }else{
    setCatSeed(newSeed)
    setCatFinal(`https://cataas.com/cat/says/${catUrl}?fontSize=50&fontColor=white`)
  }
}

function onTextChange(event: ChangeEvent<HTMLInputElement>){
  const newText = event.target.value
  setText(newText)
  setCatUrl(newText)
}

  return (
    <>
    <div className="App">
      <h1>More cats</h1>   
      <CatPicture catFinal={catFinal} catSeed={catSeed}/>
      <div className="container">
          <input placeholder="Make cat say something" value={text} onChange={onTextChange}></input>
      </div>
      <div className="container"> 
          <button className="button-36" onClick={handleClick}>New cat pls</button>
      </div>
    </div>
    </>
  )
}

export default App