import express, { Request, Response, NextFunction } from 'express'

const router = express.Router()

const students: Array<Body> = [
    {
        "id": 0,
        "name": "pekka",
        "email": "pekka1@gmail.com"
    }
]

interface Body {
    id: number
    name: string
    email: string
}

const validate = (req: Request, res: Response, next: NextFunction ) => {
    const { id: id, name, email } = req.body
    if (typeof(id) !== 'number' || typeof(name) !== 'string' || typeof(email) !== 'string') {
        return res.status(400).send('Missing or invalid parameters')
    }
    next()
}

router.post('/', validate, (req: Request, res: Response) => {
    const body: Body = req.body
    students.push(body)
    res.sendStatus(201)
})

router.get("/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    console.log("Hello from get id: ", id)
    const student = students.find(item => item.id === id)
    if (student === undefined) {
        return res.status(404).send("Error 404, invalid student id")
    }
    res.send(student)
})

router.get('/', (_req: Request, res: Response) => {
    console.log(students)
    const lista = students.map(student => {
        return student.id
    })
    res.send({ lista })
})

const validatePut = (req: Request, res: Response, next: NextFunction ) => {
    const { name, email } = req.body
    if (typeof(name) !== 'string' || typeof(email) !== 'string') {
        return res.status(400).send('Missing or invalid parameters')
    }
    next()
}

router.put('/:id', validatePut, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    console.log("Hello from put id: ", id)
    const student = students.find(item => item.id === id)
    if (student === undefined) {
        return res.status(404).send("Error 404, invalid student id")
    }else{
        students[id].name = req.body.name
        students[id].email = req.body.email
    }
    res.sendStatus(200)
})

router.delete('/:id', (req: Request, res: Response) => {
    const id = Number(req.params.id)
    console.log("Hello from delete id: ", id)
    const student = students.find(item => item.id === id)
    if (student === undefined) {
        return res.status(404).send("Error 404, invalid student id")
    }else{
        delete students[id]
    }
    res.sendStatus(200)
})

export default router