import express, { Request, Response, NextFunction } from 'express'
import argon2 from "argon2"
import { validateUserPost } from './middleware'

const router = express.Router()

const users: Array<User> = [
    {
        "name": "pekka",
        "password": "$argon2id$v=19$m=65536,t=3,p=4$X5mogx7znvNBQ8nl+UHeNg$1jyMNjBohIExt+nUAxcgthehlttxTyImIjyyfRx9BGo"
    }
]

interface User {
    name: string
    password: string
}

router.post('/register', validateUserPost, async(req: Request, res: Response) => {
    //console.log("Hello from usersRouter POST /register")
    const hashedPassword = await argon2.hash(req.body.password)
    const user: User = {name: req.body.name, password: hashedPassword}
    users.push(user)
    console.log(users)
    res.sendStatus(201)
})

router.post('/login', async(req: Request, res: Response) => {
    const { name, password } = req.body
    console.log(name, password)
    const user = users.find(user => user.name === req.body.name)
    if(user === undefined){
        return  res.status(401).send("Invalid username or password")
    }    
    const hash = user.password
    const pw = req.body.password
    const isCorrectPassword = await argon2.verify(hash, pw)
    if(!isCorrectPassword){
        return res.status(401).send("Invalid username or password")
    }

    console.log("nice")
    res.sendStatus(204)
})

router.post('/admin', async(req: Request, res: Response) => {
    const { name, password } = req.body
    const { ADMIN_NAME, ADMIN_PASSWORD} = process.env
    if(ADMIN_NAME === undefined || ADMIN_PASSWORD === undefined){
        return  res.status(401).send("Invalid username or password")
    }    
    const hash = ADMIN_PASSWORD
    const pw = password
    const isCorrectPassword = await argon2.verify(hash, pw)
    if(!isCorrectPassword || name != ADMIN_NAME){
        return res.status(401).send("Invalid username or password")
    }

    console.log("nice")
    res.status(204).send("Welcome, admin")
})


export default router