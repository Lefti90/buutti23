import jwt from "jsonwebtoken"

const input = process.argv[2] || "default"

const payload = {username: "jeps"}
const secret = input
const options = {expiresIn: "15m"}

const token = jwt.sign(payload, secret, options)
console.log(token)

try{
    const decodedToken = jwt.verify(token, secret)
    console.log(decodedToken)
    console.log(token)
    console.log(secret)
}catch(error){
    console.log("Invalid token")
}
