import express, { Request, Response, NextFunction } from 'express'
import argon2 from "argon2"
import { validateUserPost } from './middleware'

const router = express.Router()

const users: Array<User> = [
    {
        "name": "pekka",
        "password": "salasana"
    }
]

interface User {
    name: string
    password: string
}

router.post('/', validateUserPost, async(req: Request, res: Response) => {
    //console.log("Hello from usersRouter POST /register")
    const hashedPassword = await argon2.hash(req.body.password)
    const user: User = {name: req.body.name, password: hashedPassword}
    users.push(user)
    console.log(users)
    res.sendStatus(201)
})


export default router