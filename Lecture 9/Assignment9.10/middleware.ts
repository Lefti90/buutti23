import { Request, Response, NextFunction } from "express"

const logger = (req: Request, _res: Response, next: NextFunction) => {
    console.log("Hello from middleware")
    console.log(req.body)
    next()
}

const validateBook = (req: Request, res: Response, next: NextFunction ) => {
    const { id, name, author } = req.body
    if (typeof(id) !== "number" || typeof(name) !== "string" || typeof(author) !== "string") {
        return res.status(400).send("Missing or invalid parameters")
    }
    next()
}

const validateUser = (req: Request, res: Response, next: NextFunction ) => {
    const {username, password } = req.body
    if (typeof(username) !== "string" || typeof(password) !== "string") {
        return res.status(400).send("Missing or invalid parameters 2")
    }
    next()
}

export const authenticate = (req: Request, res: Response, next: NextFunction ) => {
    const auth = req.get("Authorization")
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Invalid token")
    }
    const token = auth.substring(7)
    const secret = process.env.BOOKSECRET ?? ""
    try {
        //const decodedToken = jwt.verify(token, secret)
        //console.log("DECODED TOKEN", decodedToken)
        console.log("SECRET:", secret)
        //req.user = decodedToken
        if(token === process.env.BOOKSECRET){
            next()
        }else{
            console.log("FAIL")
            res.status(400).send("Invalid token 1")
        }
        
    } catch (error) {
        console.log("SECRET:", secret)
        console.log("REQ", req.body, auth, token)
        return res.status(401).send("Invalid token")
    }
}

const unkownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send("Error 404")
}

export default logger
export { unkownEndpoint, validateBook, validateUser }