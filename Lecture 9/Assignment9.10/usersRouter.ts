import express, { Request, Response } from "express"
import { validateUser } from "./middleware"
import argon2 from "argon2"
import jwt from "jsonwebtoken"
import "dotenv/config"

const router = express.Router()

const users: Array<User> = [
    {
        "id": 0,
        "username": "pekka",
        "password": "$argon2id$v=19$m=65536,t=3,p=4$qVZABOM+LBbo1sVft12F4A$V6qOOgEjacqsNPzD7jChDa/1uEwSDIrto42LY0JXbsw"
    },
    {
        "id": 1,
        "username": "peppi",
        "password": "$argon2id$v=19$m=65536,t=3,p=4$1XEUOIAeGCaaXvIczXy7zA$63VpWBeiS9GqDzr8nLEQGO/l4sjD++dqa0EKHabpNw4"
    }
]

interface User {
    id: number
    username: string
    password: string
}

router.post("/register", validateUser, async(req: Request, res: Response) => {
    console.log("Hello from usersRouter POST /register")
    const existingUser = users.find(user => user.username === req.body.username)
    if(existingUser?.username === req.body.username){
        return res.status(400).send(`Username ${req.body.username} already exists`)
    }
    
    const hashedPassword = await argon2.hash(req.body.password)
    console.log(hashedPassword)
    const user: User = {id: users.length, username: req.body.username, password: hashedPassword}
    users.push(user)
    console.log(users)

    //create jwt on register
    const payload = {username: req.body.username}
    const secret = req.body.password
    const options = {expiresIn: "30d"}
    const token = jwt.sign(payload, secret, options)
    console.log(token)

    res.status(200).send(`Registered user: ${req.body.username} with token: ${token}`) 
})


router.post("/login", async(req: Request, res: Response) => {
    const { username, password } = req.body
    console.log(username, password)
    const user = users.find(user => user.username === req.body.username)
    if(user === undefined){
        return  res.status(401).send("Invalid username or password")
    }    
    const hash = user.password
    const pw = req.body.password
    const isCorrectPassword = await argon2.verify(hash, pw)
    if(!isCorrectPassword){
        return res.status(401).send("Invalid username or password")
    }
    
    //verify jwt on login
    const token = process.env.BOOKSAPI_USER_TOKEN ?? ""//"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBla2thIiwiaWF0IjoxNzA1NDI4NjMzLCJleHAiOjE3MDgwMjA2MzN9.Vam-Q3-X3go8qX1mmm6UKSGZzfBJMXFqVSc8K7lEiKE"
    const secret = pw
    try{
        const decodedToken = jwt.verify(token, secret)
        console.log(decodedToken)
    }catch(error){
        return res.status(401).send(`Invalid token ${token}, secret ${secret}`)
    }

    res.status(200).send(`Logged in as ${username} with token:${token}`)
})

export default router