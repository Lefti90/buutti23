import request from "supertest"
import server from "../server"

test("dummy test", () => {
    expect(true).toBe(true)
})

describe("Server", () => {
    it("Returns 404 on invalid address", async () => {
        const response = await request(server)
            .get("/invalidaddress")
        expect(response.statusCode).toBe(404)
    })
    it("Returns 200 on valid address", async () => {
        const response = await request(server)
            .get("/api/v1/books")
            .auth("supersecret", { type: "bearer" })
        expect(response.statusCode).toBe(200)
    })
})


describe("Users/Registering", () => {
    it("Registering fails on ADMIN", async () => {
        const response = await request(server)
            .post("/api/v1/users/register")
            .auth("supersecret", { type: "bearer" })
            .send({username: "admin", password: "salasana"})
        expect(response.statusCode).toBe(400)
    })

    it("Registering success on NEWUSER", async () => {
        const response = await request(server)
            .post("/api/v1/users/register")
            .auth("supersecret", { type: "bearer" })
            .send({username: "newuser", password: "salasana"})
        expect(response.statusCode).toBe(200)
    })
})

describe("Users/Login", () => {
    it("Logging in fails on wrong password", async () => {
        const response = await request(server)
            .post("/api/v1/users/login")
            .auth("supersecret", { type: "bearer" })
            .send({username: "admin", password: "wrongpassword"})
        expect(response.statusCode).toBe(401)
    })

    it("Logging in success with ADMIN", async () => {
        const response = await request(server)
            .post("/api/v1/users/login")
            .auth("supersecret", { type: "bearer" })
            .send({username: "admin", password: "salasana"})
        expect(response.statusCode).toBe(200)
    })
})

const adminToken = process.env.BOOKSAPI_ADMIN_TOKEN ?? ""
describe("Books/CRUD with ADMIN", () => {
    it("POSTS a new book", async () => {
        const response = await request(server)
            .post("/api/v1/books")
            .auth(adminToken, { type: "bearer" })
            .send({
                "id": 2,
                "name": "Lord of the rings: Return of the king",
                "author": "J.R.R Tolkien",
                "read": false
            })
        expect(response.statusCode).toBe(201)
    })

    it("POSTS a new book", async () => {
        const response = await request(server)
            .put("/api/v1/books/1")
            .auth(adminToken, { type: "bearer" })
            .send({
                "id": 1,
                "name": "Lord of the rings: Return of the towers 2 wat",
                "author": "J.R.R Tolkien",
                "read": false
            })
        expect(response.statusCode).toBe(200)
    })

    it("DELETES a book", async () => {
        const response = await request(server)
            .delete("/api/v1/books/0")
            .auth(adminToken, { type: "bearer" })
        expect(response.statusCode).toBe(200)
    })
})