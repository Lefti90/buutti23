import express from "express"
import { unkownEndpoint } from "./middleware"
import helmet from "helmet"
import booksRouter from "./booksRouter"
import usersRouter from "./usersRouter"

const server = express()
server.use(express.json())
server.use(helmet())
server.use("/api/v1/books", booksRouter)
server.use("/api/v1/users", usersRouter)
server.use(unkownEndpoint)

export default server