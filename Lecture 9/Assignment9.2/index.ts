import express from 'express'
import logger, { unkownEndpoint } from './middleware'
import studentRouter from "./studentRouter"

const server = express()
server.use(express.json())
server.use(express.static('public'))
server.use(logger)
server.use("/students", studentRouter)


server.use(unkownEndpoint)
const port = 3000
server.listen(port, () => {
    console.log('Server listening port', port)
})