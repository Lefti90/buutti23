import request from "supertest"
import server from "../server"

test('dummy test', () => {
    expect(true).toBe(true)
})

describe('Server', () => {
    it('Returns 404 on invalid address', async () => {
        const response = await request(server)
            .get('/invalidaddress')
        expect(response.statusCode).toBe(404)
    })
    it('Returns 200 on valid address', async () => {
        const response = await request(server)
            .get('/')
        expect(response.statusCode).toBe(200)
    })
})

// describe("Users", () => {
//     it('Returns 200 on valid address', async () => {
//         const response = await request(server)
//             .post("/users/register")
//             .send({ "name": "ilkka", "password": "salasana" })
//             .auth("token", "verysecret")
//         expect(response.statusCode).toBe(200)
//     })
// })

describe('/users/register', () => {
    it('returns token', async () => {
        const response = await request(server)
            .post('/users/register')
            .send({ name: 'testuser2', password: 'password2' })
        expect(response.statusCode).toBe(200)
        expect(response.body).toBeDefined()
    })
})
