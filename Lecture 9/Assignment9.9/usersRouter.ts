import express, { Request, Response, NextFunction } from 'express'
import argon2 from "argon2"
import { validateUserPost, authenticate } from './middleware'
import jwt from "jsonwebtoken"

const router = express.Router()

const users: Array<User> = [
    {
        "name": "pekka",
        "password": "$argon2id$v=19$m=65536,t=3,p=4$X5mogx7znvNBQ8nl+UHeNg$1jyMNjBohIExt+nUAxcgthehlttxTyImIjyyfRx9BGo"
    }
]

interface User {
    name: string
    password: string
}

router.post('/register', validateUserPost, authenticate, async(req: Request, res: Response) => {
    //console.log("Hello from usersRouter POST /register")
    const hashedPassword = await argon2.hash(req.body.password)
    const user: User = {name: req.body.name, password: hashedPassword}
    users.push(user)
    console.log(users)

    //create jwt on register
    const payload = {username: req.body.name}
    const secret = req.body.password
    const options = {expiresIn: "30d"}
    const token = jwt.sign(payload, secret, options)
    console.log(token)

    res.status(200).send(`Registered user: ${req.body.name} with token: ${token}`)
})

router.post('/login', authenticate, async(req: Request, res: Response) => {
    const { name, password } = req.body
    console.log(name, password)
    const user = users.find(user => user.name === req.body.name)
    if(user === undefined){
        return  res.status(401).send("Invalid username or password")
    }    
    const hash = user.password
    const pw = req.body.password
    const isCorrectPassword = await argon2.verify(hash, pw)
    if(!isCorrectPassword){
        return res.status(401).send("Invalid username or password")
    }

    //verify jwt on login
    const token = process.env.STUDENT_USER_TOKEN ?? ""
    const secret = pw
    try{
        const decodedToken = jwt.verify(token, secret)
        console.log(decodedToken)
    }catch(error){
        return res.status(401).send(`Invalid token ${token}, secret ${secret}`)
    }

    res.status(200).send(`Logged in as ${name} with token: ${token} secret: ${secret}`)
})

router.post('/admin', async(req: Request, res: Response) => {
    const { name, password } = req.body
    const { ADMIN_NAME, ADMIN_PASSWORD} = process.env
    if(ADMIN_NAME === undefined || ADMIN_PASSWORD === undefined){
        return  res.status(401).send("Invalid username or password")
    }    
    const hash = ADMIN_PASSWORD
    const pw = password
    const isCorrectPassword = await argon2.verify(hash, pw)
    if(!isCorrectPassword || name != ADMIN_NAME){
        return res.status(401).send("Invalid username or password")
    }

    console.log("nice")
    res.status(204).send("Welcome, admin")
})


export default router