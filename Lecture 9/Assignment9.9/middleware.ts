import express, { Request, Response, NextFunction } from 'express'
import jwt from "jsonwebtoken"

const router = express.Router()

const logger = (req: Request, res: Response, next: NextFunction) => {
    // console.log("Logger middleware: ")
    // const dateTime = new Date()
    // console.log(dateTime)
    // console.log(req.method)
    // console.log(req.url)
    // console.log(req.body)
    // console.log("_______")
    next()
}

const unkownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send("Error 404")
}

export default logger
export { unkownEndpoint }

export const validateUserPost = (req: Request, res: Response, next: NextFunction ) => {
    const { name, password } = req.body
    if (typeof(name) !== 'string' || typeof(password) !== 'string') {
        console.log("validateUserPost 1")
        return res.status(400).send('Missing username or password')
    }
    if(name === "" || password === ""){
        console.log("validateUserPost 2")
        return res.status(400).send('Invalid username or password')
    }
    next()
}

export const authenticate = (req: Request, res: Response, next: NextFunction ) => {
    const auth = req.get('Authorization')
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('Invalid token')
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET ?? ""
    try {
        //const decodedToken = jwt.verify(token, secret)
        //console.log("DECODED TOKEN", decodedToken)
        console.log("SECRET:", secret)
        //req.user = decodedToken
        if(token === process.env.SECRET){
            next()
        }else{
            console.log("FAIL")
        }
        
    } catch (error) {
        console.log("SECRET:", secret)
        console.log("REQ", req.body, auth, token)
        return res.status(401).send('Invalid token')
    }
}

router.get('/protected', authenticate, (req: Request, res: Response) => {
    //res.send(`${req.user.username} accessed protected route`)
})
