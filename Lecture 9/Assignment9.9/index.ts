import express from 'express'
import logger, { unkownEndpoint, authenticate } from './middleware'
import studentsRouter from "./studentsRouter"
import usersRouter from "./usersRouter"

const server = express()
server.use(express.json())
server.use(express.static('public'))
server.use(logger)
server.use("/students", studentsRouter)
server.use("/users", usersRouter)


server.use(unkownEndpoint)

const port = 3000
server.listen(port, () => {
    console.log('Server listening port', port)
})