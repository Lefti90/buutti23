import jwt from "jsonwebtoken"

const input = process.argv[2] || "default"

const payload = {username: "jeps"}
const secret = input
const options = {expiresIn: "15m"}

const token = jwt.sign(payload, secret, options)
console.log(token)