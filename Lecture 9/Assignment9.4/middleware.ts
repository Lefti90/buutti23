import { Request, Response, NextFunction } from 'express'

const logger = (req: Request, res: Response, next: NextFunction) => {
    console.log("Logger middleware: ")
    const dateTime = new Date()
    console.log(dateTime)
    console.log(req.method)
    console.log(req.url)
    console.log(req.body)
    console.log("_______")
    next()
}

const unkownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send("Error 404")
}

export default logger
export { unkownEndpoint }

export const validateUserPost = (req: Request, res: Response, next: NextFunction ) => {
    const { name, password } = req.body
    if (typeof(name) !== 'string' || typeof(password) !== 'string') {
        return res.status(400).send('Missing username or password')
    }
    if(name === "" || password === ""){
        return res.status(400).send('Invalid username or password')
    }
    next()
}