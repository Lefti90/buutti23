// Assignment 4.8: map
// Write a function that takes a list of strings and returns a list, 
// where those strings are reversed.

// With the example input the result should be
//     ['ytic', 'etubirtsid', …, ]

const words = [
    'city',
    'distribute',
    'battlefield',
    'relationship',
    'spread',
    'orchestra',
    'directory',
    'copy',
    'raise',
    'ice'
]


function reverseString(str: string) {
    // Step 1. Use the split() method to return a new array
    var splitString = str.split(""); // var splitString = "hello".split("");
    // ["h", "e", "l", "l", "o"]
 
    // Step 2. Use the reverse() method to reverse the new created array
    var reverseArray = splitString.reverse(); // var reverseArray = ["h", "e", "l", "l", "o"].reverse();
    // ["o", "l", "l", "e", "h"]
 
    // Step 3. Use the join() method to join all elements of the array into a string
    var joinArray = reverseArray.join(""); // var joinArray = ["o", "l", "l", "e", "h"].join("");
    // "olleh"
    
    //Step 4. Return the reversed string
    return joinArray; // "olleh"
}

const reverseWords = words.map((word) => reverseString(word))
console.log(reverseWords)