// Assignment 4.12: Array Manipulation
// const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
// From the elements of this array,

const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]


// Create a new array with only the numbers that are divisible by three.
const div3 = arr.map((num) => {
    let index = 0
    if(num % 3 === 0){
        return num
    }else{
        arr.splice(index, 1)
    }
    index++
})

const div3Filtered = div3.filter((num) => {
    if(num == undefined){
        return false
    }else{
        return true
    }
})

console.log(div3Filtered)

// Create a new array from original array (arr), where each number is multiplied by 2

const timesTwo = arr.map((num) => num * 2)

console.log(timesTwo)

// Sum all of the values in the array using the array method reduce

const sum = arr.reduce((acc, cur) => {
    return acc + cur
},0)

console.log(sum)