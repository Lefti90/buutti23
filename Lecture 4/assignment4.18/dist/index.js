"use strict";
// Assignment 4.18: Palindrome
// Check if given string is a palindrome.
// Examples:
// npm start saippuakivikauppias -> Yes, 'saippuakivikauppias' is a palindrome npm start 
// saippuakäpykauppias -> No, 'saippuakäpykauppias' is not a palindrome
const input = process.argv[2] || "saippuakivikauppias";
const reversed = input.split("").reverse().join("");
if (input === reversed)
    console.log("Yes", input, "is a palindrome");
else
    console.log("No", input, "is not a palindrome");
