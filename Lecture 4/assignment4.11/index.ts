// Assignment 4.11: reduce
// Create a function that takes two parameters: a list of strings and a separator string. 
// The function should join all the strings in the array, with the separator string between 
// each element, and then return the result.

// You must use reduce. You can not use .join

const characters = ["a", "b", "c", "d"]

function joiner(list: Array<String>, separator: string){
    const reducedString = list.reduce((acc, cur) => {
        return acc + separator + cur
    })
    console.log(reducedString)
}

joiner(characters, ",")