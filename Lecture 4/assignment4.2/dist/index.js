"use strict";
// Write a function that generates a random number within range, rounded to nearest integer. 
// Function takes in two numbers, min and max.
// Hint: Math
// Note: It is a common convention to have the starting point of a range to be inclusive, 
// and the ending point exclusive. For example the substring function takes two parameters
//  following this convention. Following this logic will make the use of the Math library easier.
function randomRoundedInt(num1, num2) {
    console.log(num1, num2);
    const randomBeforeRounding = Math.random() * (num2 - num1) + num1;
    console.log(randomBeforeRounding);
    return Math.floor(randomBeforeRounding);
}
const finalNumber = randomRoundedInt(5, 10);
console.log(finalNumber);
