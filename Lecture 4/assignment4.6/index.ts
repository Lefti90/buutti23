// Write a program that calculates the factorial n of a given number n using recursion.

// Definition of factorial of n, written as n!, is

// n! = n * (n-1) * (n-2) * ... * 1

// So for example, if n = 4, then n! = 4 * 3 * 2 * 1 = 24

let answer = 0
function factorial(num: number){
    for (let i = num; i > 1; i--) {
        const curNum = num * (i-1)
        console.log(num, "*", i - 1, "=", curNum)
        answer = answer + curNum
    }
}

factorial(4)
console.log("Answer is : ", answer)