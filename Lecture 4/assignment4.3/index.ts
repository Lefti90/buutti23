// Write a function that takes either 2 or 3 parameters, 
// then calculates the sum of these parameters and returns the result.

// Do this three times: 
// Using a named function
function calculateSum(num1: number, num2: number, num3: number = 0){
    return num1 + num2 + num3
}

// Using an anonymous function
const calculateSumAno = function (num1: number, num2: number, num3: number = 0){
    return num1 + num2 + num3
}

// Using an arrow function
const calcSumAnon = (num1: number, num2: number, num3: number = 0) => {
    return num1 + num2 + num3
}



console.log(calculateSum(1, 2, 3))
console.log(calculateSumAno(2, 3))
console.log(calcSumAnon(3, 4, 5))