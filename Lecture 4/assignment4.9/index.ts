// Assignment 4.9: filter
// Write a function that takes a list of numbers and returns a list with all the items 
// from the input list that are divisible by three or five, but are not divisible by both.

// With the example input the result should be
// [ 498654, 234534, 365457, 873453 ]

const numbers = [
    749385,
    498654,
    234534,
    345467,
    956876,
    365457,
    235667,
    464534,
    346436,
    873453
]

const divisibleNumbers = numbers.filter((number) => {
    if((number % 3 === 0 || number % 5 === 0) && !(number % 3 === 0 && number % 5 === 0))
        return true
})

console.log(divisibleNumbers)