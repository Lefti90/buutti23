// Assignment 4.17: Reversed Words
// Create a programs that reverses each word in a string.

// node .\reversed_words.js "this is a very long sentence" -> sihT si a yrev gnol ecnetnes

const input = process.argv[2] || "this is a very long sentence"
const newWords : Array<string> = []
let reverseWord = ""
let reversedString = ""

function flipWords(str: string){
    const words = str.split(" ")
    console.log(words)
    words.forEach(word => {
        reverseWord = word.split("").reverse().join("")
        //console.log(reverseWord)
        newWords.push(reverseWord)
        //console.log(newWords)
        reversedString = newWords.join(" ")
    })
    return reversedString
}

console.log(flipWords(input))