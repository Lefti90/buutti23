// Assignment 4.10: find
// Find the first name in the list that is three letters long and ends in letter t.

const names = [
    'Murphy',
    'Hayden',
    'Parker',
    'Arden',
    'George',
    'Andie',
    'Ray',
    'Storm',
    'Tyler',
    'Pat',
    'Keegan',
    'Carroll'
]

const thisName = names.find((name) => {
    if(name.length == 3 && name.charAt(name.length-1) == "t")
        return true
})

console.log(thisName)