"use strict";
// Assignment 4.20: Fibonacci Sequence
// The Fibonacci sequence is a sequence of numbers where each number is the sum of 
// the two preceding ones, starting from 0 and 1. Create a function 
// that produces a Fibonacci sequence of length n.
// For example, if n = 8, the program should produce a sequence [0,1,1,2,3,5,8,13].
const input = process.argv[2] || "8";
function calcFibonacci(num1) {
    let numA = 0;
    let numB = 1;
    let curNum = 0;
    const fiboArr = [0, 1];
    for (let i = 0; i < num1 - 2; i++) {
        curNum = numA + numB;
        numA = numB;
        numB = curNum;
        fiboArr.push(curNum);
    }
    return fiboArr;
}
console.log(calcFibonacci(parseInt(input)));
