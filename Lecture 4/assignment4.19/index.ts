// Assignment 4.19: Array Randomizer
// const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
// Create a program that every time you run it, prints out an array with
// differently randomized order of the array above.

// Example:
// npm start -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]

const array : Array<number>= [2, 4, 5, 6, 8, 10, 14, 18, 25, 32]

//help from the internets
//https://bost.ocks.org/mike/shuffle/

function shuffle(arr : Array<number>) {
    let len = arr.length
    let curElement 
    let element
  
    // While there remain elements to shuffle…
    while (len) {
  
        // Pick a remaining element…
        element = Math.floor(Math.random() * len--)
  
        // And swap it with the current element.
        curElement = arr[len]
        arr[len] = arr[element]
        arr[element] = curElement
    }
  
    return arr
}

console.log(shuffle(array))
