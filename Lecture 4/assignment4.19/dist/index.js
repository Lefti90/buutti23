"use strict";
// Assignment 4.19: Array Randomizer
// const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
// Create a program that every time you run it, prints out an array with
// differently randomized order of the array above.
// Example:
// npm start -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]
const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
// function shuffle(arr : Array<number>){
//    const randomizedArray : Array<number> = []
//     for (let i = arr.length - 1; i > 0; i--) {
//         const j = Math.floor(Math.random() * (i + 1))
//         const temp = arr[i]
//         arr[i] = arr[j]
//         arr[j] = temp
//         randomizedArray.push(temp)
//     }
//     return randomizedArray
// }
function shuffle(arr) {
    let m = arr.length;
    let t;
    let i;
    // While there remain elements to shuffle…
    while (m) {
        // Pick a remaining element…
        i = Math.floor(Math.random() * m--);
        // And swap it with the current element.
        t = arr[m];
        arr[m] = arr[i];
        arr[i] = t;
    }
    return arr;
}
console.log(shuffle(array));
