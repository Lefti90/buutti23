"use strict";
// Assignment 4.13: Count Sheep
// Create a program that takes in a number from the command line, 
// for example npm start 3 and prints a string "1 sheep...2 sheep...3 sheep..."
const input = process.argv[2];
const inputToNum = parseInt(input);
let sheepString = "";
for (let i = 0; i < inputToNum; i++) {
    sheepString = sheepString + `${i + 1} sheep...`;
}
console.log(sheepString);
