// Assignment 4.21: Prime Numbers
// A prime number (or a prime) is a natural number (aka positive integer) 
// greater than 1 that is not a product of two smaller natural numbers.
//  For example, 4 is not a prime because it is divisible by 2.
//   On the other hand, 7 is a prime: it is only divisible by 7 and 1.

// Create a function that checks whether a given number is a prime number.

const input = process.argv[2] || "7"

function calcPrime(num : number){
    let isPrime = true
    for (let i = 2; i < num; i++) {
        if(num % i == 0){
            isPrime = false
            break
        }
    }
    if(isPrime){
        console.log(num, "is a prime number")
    }else{
        console.log(num, "is not a prime number")
    }
}

calcPrime(parseInt(input))