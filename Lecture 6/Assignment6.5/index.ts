import axios from "axios"

interface Movie {
    Title: string
    Year: string
}

async function getMovie(name: string, year?: number){
    const movie: Movie[] = (await axios.get(`https://omdbapi.com/?apikey=c3a0092f&s=${name}&y=${year}`)).data.Search
    //console.log("Title: ", movie[0].Title, "Year: ", movie[0].Year)
    movie.forEach( film => console.log("Title: ", film.Title, "Year: ", film.Year))
    //console.log(movie)
}

getMovie("Shrek", 2004)