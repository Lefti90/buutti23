"use strict";
// Assignment 6.3: Random Async
// Use the following asynchronous function twice to get 2 random values. 
// After getting both values, console.log() them.
// const getValue = function (): Promise<number> {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(Math.random())
//         }, Math.random() * 1500)
//     })
// }
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// Do this exercise twice. First time, use async & await, and on the second time use promise.then().
//Async & await
const getValue = function () {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random());
        }, Math.random() * 1500);
    });
};
const printAfterDelay = () => __awaiter(void 0, void 0, void 0, function* () {
    const x = (yield getValue());
    const y = (yield getValue());
    console.log(x);
    console.log(y);
});
printAfterDelay();
//promise
let result1;
getValue()
    .then(result => {
    result1 = result;
    return getValue();
})
    .then((result2) => console.log(result1 + "\n" + result2));
