// Assignment 6.3: Random Async
// Use the following asynchronous function twice to get 2 random values. 
// After getting both values, console.log() them.
// const getValue = function (): Promise<number> {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(Math.random())
//         }, Math.random() * 1500)
//     })
// }

// Do this exercise twice. First time, use async & await, and on the second time use promise.then().


//Async & await
const getValue = function (): Promise<number> {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}
const printAfterDelay = async () => {
    const x = (await getValue())
    const y = (await getValue())

    console.log(x)
    console.log(y)
}
printAfterDelay()

//promise
let result1: number
getValue()
    .then(result => {
        result1 = result 
        return getValue()
    })
    .then((result2) => console.log(result1 + "\n" + result2))