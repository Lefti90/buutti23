// Assignment 6.9: Race Cars

// Program a race car simulator!

// In this race we have several drivers and several laps. 
// Each lap every driver has a three percent chance of crashing. 
// If they don't crash, they finish one lap in 20-25 seconds. 
// Write a function raceLap that takes no parameters and returns a promise. 
// If the driver crashes, the promise is rejected. If not, the promise is 
// resolved and the lap time is returned.

// Write a function race that takes two parameters: a list of drivers 
// (strings), and the number of laps (number). When the race function is 
// run, it should run the raceLap function for eachd driver on every lap. 
// The race function should keep track of each drivers total time and best lap time. 
// If a driver crashes, their times are not updated on further laps. When all laps have finished,
//  the race function should return the name and stats of the winner.


const drivers = [
    {name: "Mika", laps: 0, bestLap: Infinity, alive: true},
    {name: "Kimi", laps: 0, bestLap: Infinity, alive: true},
    {name: "Ismo", laps: 0, bestLap: Infinity, alive: true},
    {name: "Seppo", laps: 0, bestLap: Infinity, alive: true}
]

interface Drivers {
    name: string
    laps: number
    bestLap: number
    alive: boolean
}

function raceLap(){
    return new Promise<number | false>((resolve, reject) =>{
        const crashChance = Math.random()
        let didntCrash = true
        let lapTime = 0
        if(crashChance > 0.3){
            didntCrash = true
            lapTime = Math.random() * (25 - 20) + 20
            return resolve(lapTime)
        }else{
            didntCrash = false
            return reject(didntCrash)
        }
    })
}

async function race(drivers: Array<Drivers>, laps: number){
    let winnerTime = Infinity
    let winner

    // Loop through all drivers
    await Promise.all(drivers.map(async (driver) => {
        driver.laps = laps

        // Loop through all laps
        for (let i = 0; i < driver.laps; i++) {
            try {
                const lapStatus = await raceLap()
                if (lapStatus !== false && driver.alive == true) {
                    if (lapStatus < driver.bestLap) {
                        driver.bestLap = lapStatus
                    }
                } else {
                    driver.alive = false
                    driver.laps = 0 // Set driver.laps to 0 if crashed
                    break // Break out of the lap loop if crashed
                }
            } catch (error) {
                console.error(driver.name, "crashed. Race finished:", error)
            }
        }

        // Check if this driver is the winner
        if (driver.bestLap < winnerTime) {
            winnerTime = driver.bestLap
            winner = driver
        }
    }))

    console.log(winner)
    return winner
}

race(drivers, 3)
console.log(drivers)


// raceLap()
//     .then(result => console.log(result))
//     .catch(error => console.log(error))

// async function race(drivers: Array<object>, laps: number){
//     drivers.map(async (driver) => {
//         if(await raceLap().then(result => (result)) == false){
//             laps--
//             console.log(laps)
//         }
//     })
// }