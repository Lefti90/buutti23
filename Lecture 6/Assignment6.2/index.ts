// Assignment 6.2: Promise Countdown
// Create a countdown program using the setTimeout function and promises.

// The program should output something like this.

// 3 ⇒ Wait 1 second
// 2 ⇒ Wait another second
// 1 ⇒ Wait the last 1 second..
// GO!

console.log("Starting countdown...")

new Promise<void>((resolve, reject) =>{
    const countDown = true
    if(countDown){
        resolve()
    }else{
        reject("countdown was false")
    }
})
    .then(() => {
        setTimeout(() => {
            console.log(3)
            setTimeout(() => {
                console.log(2)
                setTimeout(() => {
                    console.log(1)
                    setTimeout(() => {
                        console.log("GO!")
                    }, 1000)
                }, 1000)
            }, 1000)
        }, 1000)
    })
    .catch((error) => {
        console.log("Error: ", error)
    })