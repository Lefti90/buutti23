"use strict";
// Assignment 6.1: Callback Countdown
// Create a countdown program using the setTimeout function and callbacks.
// The program should output something like this.
// 3   ⇒ Wait 1 second
// 2   ⇒ Wait another second
// 1   ⇒ Wait the last 1 second..
// GO!
const countDown = function (time, func) {
    setTimeout(() => {
        func();
    }, time);
};
console.log("Start:");
countDown(1000, () => {
    console.log("3");
    countDown(1000, () => {
        console.log("2");
        countDown(1000, () => {
            console.log("1");
            countDown(1000, () => {
                console.log("GO!");
            });
        });
    });
});
