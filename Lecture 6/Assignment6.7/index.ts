// Assignment 6.7: Student Grades

// Find the highest, and the lowest scoring students. 
// Then find the average score of the students. 
// Print out only the students who scored higher than the average. 
// Assign grades (1-5) to all students based on their scores 
// "1": "1-39", "2": "40-59", "3": "60-79", "4": "80-94", "5": "95-100"

interface Student {
    name: string
    score: number
    grade?: number
}

const students: Student[] = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
]

function findHighest(arr: Student[]){
    let highestScore = 0
    let highestScorer: Student | null = null

    arr.forEach((student) => {
        if(student.score > highestScore){
            highestScore = student.score
            highestScorer = student
        }
    })
    if (highestScorer !== null) {
        console.log(highestScorer)
    } else {
        console.log("No student found")
    }
}

function findLowest(arr: Student[]){
    let lowestScore = arr[0].score
    let lowestScorer: Student | null = null

    arr.forEach((student) => {
        if(student.score < lowestScore){
            lowestScore = student.score
            lowestScorer = student
        }
    })
    if (lowestScorer !== null) {
        console.log(lowestScorer)
    } else {
        console.log("No student found")
    }
}

function average(arr: Student[]){
    let sum = 0
    let amount = 0
    arr.forEach((student) => {
        sum += student.score
        amount++
    })
    const average = sum/amount
    console.log(average)
}

function getGrades(arr: Student[]){
    arr.map((student) => {
        if(student.score >= 1 && student.score <= 39){            
            student.grade = 1
        }else if(student.score >= 40 && student.score <= 59){
            student.grade = 2
        }else if(student.score >= 60 && student.score <= 79){
            student.grade = 3
        }else if(student.score >= 80 && student.score <= 94){
            student.grade = 4
        }else if(student.score >= 95 && student.score <= 100){
            student.grade = 5
        }else{
            student.grade = 0
        }
    })
}

findHighest(students)
findLowest(students)
average(students)
getGrades(students)
console.log(students)