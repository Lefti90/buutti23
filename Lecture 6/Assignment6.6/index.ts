// Assignment 6.6: Largest Number

//     Create a function that finds the largest number in an array.
//     Create a function that finds the second largest number in an array.

// Do this assignment without first sorting the array,
//  or using any functions from the Math module or external libraries.

const array1 = [1, 30, 4, 21, 100000, 100]
let largestNumber: number = 0
let secondLargestNumber: number = 0
function findLargest(numberArray: Array<number>){
    for (let i = 0; i < numberArray.length; i++) {
        if(numberArray[i] > largestNumber){
            largestNumber = numberArray[i]
        }
    }
    return largestNumber
}

function findSecondLargest(numberArray: Array<number>){
    findLargest(numberArray)
    for (let i = 0; i < numberArray.length; i++) {
        if(numberArray[i] > secondLargestNumber && numberArray[i] < largestNumber){
            secondLargestNumber = numberArray[i]
        }
    }
    return secondLargestNumber
}

console.log(findLargest(array1))
console.log(findSecondLargest(array1))