"use strict";
// Assignment 6.8: Todos
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Use the axios library (or fetch) to fetch data from 
// ‘https://jsonplaceholder.typicode.com/todos/’
//     Console log out that data
//     Modify the existing data by also fetching the user from 
//     ‘https://jsonplaceholder.typicode.com/users/${userId}’ 
//     and adding it to the TODO-thing where the userId is from, 
//     and remove the userId from that data item
//     Modify in the resulting array of objects the ‘user’ field 
//     of every object to only contain the fields ‘name’, ‘username’, and ‘email’.
//     Modify the functionality so that you separately fetch all users from 
//     'https://jsonplaceholder.typicode.com/users/' and all todos from 
//     'https://jsonplaceholder.typicode.com/todos/', and solve the 
//     tasks 2-4 by using only the data from the two arrays you fetched, 
//     effectively reducing the amount of requests to the API to 2.
const axios_1 = __importDefault(require("axios"));
function getUsers() {
    return __awaiter(this, void 0, void 0, function* () {
        const todos = (yield axios_1.default.get("https://jsonplaceholder.typicode.com/todos/")).data;
        const todoArray = todos.map((todo) => __awaiter(this, void 0, void 0, function* () {
            const user = (yield axios_1.default.get(`https://jsonplaceholder.typicode.com/users/${todo.userId}`)).data;
            return { id: todo.id,
                title: todo.title,
                completed: todo.completed,
                user: {
                    name: user.name,
                    username: user.username,
                    email: user.email
                },
            };
        }));
        return Promise.all(todoArray);
    });
}
function getUsers2() {
    return __awaiter(this, void 0, void 0, function* () {
        const todos = (yield axios_1.default.get("https://jsonplaceholder.typicode.com/todos/")).data;
        const users = (yield axios_1.default.get("https://jsonplaceholder.typicode.com/users/")).data;
        const todoArray = todos.map((todo) => __awaiter(this, void 0, void 0, function* () {
            const currentId = todo.userId;
            if (currentId >= 0 && currentId < users.length) {
                return { id: todo.id,
                    title: todo.title,
                    completed: todo.completed,
                    user: {
                        name: users[currentId].name,
                        username: users[currentId].username,
                        email: users[currentId].email
                    },
                };
            }
            else {
                return {
                    id: todo.id,
                    title: todo.title,
                    completed: todo.completed,
                    user: null //user not found
                };
            }
        }));
        return Promise.all(todoArray);
    });
}
getUsers2().then(result => console.log(result));
