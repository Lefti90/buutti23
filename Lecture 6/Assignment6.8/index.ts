// Assignment 6.8: Todos

// Use the axios library (or fetch) to fetch data from 
// ‘https://jsonplaceholder.typicode.com/todos/’

//     Console log out that data
//     Modify the existing data by also fetching the user from 
//     ‘https://jsonplaceholder.typicode.com/users/${userId}’ 
//     and adding it to the TODO-thing where the userId is from, 
//     and remove the userId from that data item
//     Modify in the resulting array of objects the ‘user’ field 
//     of every object to only contain the fields ‘name’, ‘username’, and ‘email’.
//     Modify the functionality so that you separately fetch all users from 
//     'https://jsonplaceholder.typicode.com/users/' and all todos from 
//     'https://jsonplaceholder.typicode.com/todos/', and solve the 
//     tasks 2-4 by using only the data from the two arrays you fetched, 
//     effectively reducing the amount of requests to the API to 2.

import axios from "axios"

interface User {
    id: number
    name: string
    email: string
    username: string
}

interface Todo {
    id: number
    title: string
    completed: boolean
    userId: number
}


// async function getUsers() {
//     const todos: Array<Todo> = (await axios.get("https://jsonplaceholder.typicode.com/todos/")).data
//     const todoArray = todos.map(async (todo) => {
//         const user: User = (await axios.get(`https://jsonplaceholder.typicode.com/users/${todo.userId}`)).data
//         return {id: todo.id,
//             title: todo.title,
//             completed: todo.completed,
//             user: {
//                 name: user.name,
//                 username: user.username,
//                 email: user.email
//             },
//         }
//     })
//     return Promise.all(todoArray)
// }

async function getUsers2() {
    const todos: Array<Todo> = (await axios.get("https://jsonplaceholder.typicode.com/todos/")).data 
    const users: Array<User> = (await axios.get("https://jsonplaceholder.typicode.com/users/")).data
    const todoArray = todos.map(async (todo) => {
        const currentId = todo.userId
        if(currentId >= 0 && currentId < users.length){
            return {id: todo.id,
                title: todo.title,
                completed: todo.completed,
                user: {
                    name: users[currentId].name,
                    username: users[currentId].username,
                    email: users[currentId].email
                },
            }
        }else{
            return {
                id: todo.id,
                title: todo.title,
                completed: todo.completed,
                user: null //user not found
            }
        }
    })
    return Promise.all(todoArray)
}


getUsers2().then(result => console.log(result))
