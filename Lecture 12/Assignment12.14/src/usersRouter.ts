import { Router } from "express"
import dao from "./dao"

const router = Router()

router.get("/", async (_req, res) => {
    const result = await dao.findAllUsers()
    res.send(result)
})

router.get("/:id", async (req, res) => {
    const id = Number(req.params.id)
    console.log(id)
    const result = await dao.getUser(id)
    res.send(result.rows)
})

router.post("/", async (req, res) =>{
    const user = req.body
    const result = await dao.insertUser(user)
    const storedUser = {id: result.rows[0], ...user}
    res.send(storedUser)
})

export default router