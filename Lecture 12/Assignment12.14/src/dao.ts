import { executeQuery } from "./db"


///////USERS
const findAllUsers = async () => {
    const query = `SELECT * FROM users`
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result.rows
}

const getUser = async (id:number) => {
    const query = `
        SELECT * FROM users
        WHERE id = ${id};
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

const insertUser = async (user: any) => {
    console.log(user)
    const query = `
        INSERT INTO users (username, full_name, email, id)
	        VALUES
		        ($1, $2, $3, $4);
    `
    const params = [...Object.values(user)]    
    console.log(`Inserting a new user ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New user inserted succesfully.`)
    return result
}

/////////////////
// POSTS

const findAllPosts = async () => {
    const query = `SELECT * FROM posts`
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result.rows
}

const getPost = async (id:number) => {
    const query = `
        SELECT * FROM posts
        WHERE postid = ${id};
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

const insertPost = async (user: any) => {
    console.log(user)
    const query = `
        INSERT INTO posts (postid, comments, title, content, post_date, username)
	        VALUES
		        ($1, $2, $3, $4, $5, $6);
    `
    const params = [...Object.values(user)]    
    console.log(`Inserting a new post ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New post inserted succesfully.`)
    return result
}

////////////////


/////////////////
// COMMENTS

const getComments = async (username: string) => {
    const query = `
        SELECT * FROM comments
        WHERE username = '${username}';
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

const insertComment = async (user: any) => {
    console.log(user)
    const query = `
        INSERT INTO comments (commentid, username, content, comment_date, postid)
	        VALUES
		        ($1, $2, $3, $4, $5);
    `
    const params = [...Object.values(user)]    
    console.log(`Inserting a new comment ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New comment inserted succesfully.`)
    return result
}

////////////////
// const updateUser = async (id: number, user:any) => {
//     console.log(id, user)
//     const query = `
//         UPDATE users
// 	    SET name = $1, price = $2
// 		WHERE userid = ${id};
//     `
//     const params = [...Object.values(user)]
//     console.log(`Inserting a new user ${params}...`)
//     const result = await executeQuery(query, params)
//     console.log(`New user updated succesfully.`)
//     return result
// }

// const deleteUser = async (id: number) => {
//     console.log(id)
//     const query = `
//         DELETE FROM users WHERE id = ${id};
//     `
//     const result = await executeQuery(query)
//     console.log(`${id} was deleted from users`)
//     return result
// }

export default { findAllUsers, insertUser, getUser,
    findAllPosts, getPost, insertPost,
    getComments, insertComment }