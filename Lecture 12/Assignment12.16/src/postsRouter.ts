import { Router } from "express"
import dao from "./dao"

const router = Router()

router.get("/", async (_req, res) => {
    const result = await dao.findAllPosts()
    res.send(result)
})

router.get("/:id", async (req, res) => {
    const id = Number(req.params.id)
    console.log(id)
    const result = await dao.getPost(id)
    res.send(result.rows)
})

router.post("/", async (req, res) =>{
    const post = req.body
    const result = await dao.insertPost(post)
    const storedPost = {id: result.rows[0], ...post}
    res.send(storedPost)
})

router.post("/:id", async (req, res) => {
    console.log("DELETE")
    const id = Number(req.params.id)
    const result = await dao.deletePost(id)
    res.sendStatus(200)    
})

router.post("/update/:id",async (req, res) => {
    console.log("UPDATE")
    const id = Number(req.params.id)
    const content = req.body.content
    const result = await dao.updatePost(id, content)
    res.sendStatus(200)
})


// router.post("/:id", async (req, res) => {
//     const product = req.body
//     const id = Number(req.params.id)
//     const _result = await dao.updateProduct(id, product)
//     const storedProduct = {product}
//     res.send(storedProduct)
// })

// router.delete("/:id", async (req, res) => {
//     const id = Number(req.params.id)
//     const _result = await dao.deleteProduct(id)
//     res.send(`${id} was deleted from products`)
// })

// router.get("/:id", async (req, res) => {
//     const id = Number(req.params.id)
//     const result = await dao.getUser(id)
//     res.send(result.rows)
// })

export default router