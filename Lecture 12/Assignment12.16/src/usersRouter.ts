import { Router } from "express"
import dao from "./dao"

const router = Router()

router.get("/", async (_req, res) => {
    const result = await dao.findAllUsers()
    res.send(result)
})

router.get("/:id", async (req, res) => {
    const id = Number(req.params.id)
    console.log(id)
    const result = await dao.getUser(id)
    res.send(result.rows)
})

router.post("/", async (req, res) =>{
    const user = req.body
    const result = await dao.insertUser(user)
    const storedUser = {id: result.rows[0], ...user}
    res.send(storedUser)
})

router.post("/:username", async (req, res) => {
    console.log("DELETE")
    const username = req.params.username
    const result = await dao.deleteUser(username)
    res.sendStatus(200)
})

router.post("/update/:username",async (req, res) => {
    console.log("UPDATE")
    const username = req.params.username
    const content = req.body.email
    const result = await dao.updateUser(username, content)
    res.sendStatus(200)
})

export default router