import { executeQuery } from "./db"


///////USERS
const findAllUsers = async () => {
    const query = `SELECT * FROM users`
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result.rows
}

const getUser = async (id:number) => {
    const query = `
        SELECT * FROM users
        WHERE id = ${id};
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

const insertUser = async (user: any) => {
    console.log(user)
    const query = `
        INSERT INTO users (username, full_name, email, id)
	        VALUES
		        ($1, $2, $3, $4);
    `
    const params = [...Object.values(user)]    
    console.log(`Inserting a new user ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New user inserted succesfully.`)
    return result
}

const deleteUser = async (username: string) => {
    const query=`
        DELETE FROM users
        WHERE username = '${username}';
    `
    //Works, but crashes if the USER still has POSTS on it left
    //So this should check if this USER still has COMMENTS and delete those first
    //Pseudocode: if queryPosts !== null -> delete from posts where username
    console.log(query)
    const result = await executeQuery(query)
    return result
}

const updateUser = async (username: string, content: string) => {
    const query =`
    UPDATE users
    SET email = '${content}'
    WHERE username = '${username}';
    `
    console.log(query)
    const result = await executeQuery(query)
    return result
}

/////////////////
// POSTS

const findAllPosts = async () => {
    const query = `SELECT * FROM posts`
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result.rows
}

const getPost = async (id:number) => {
    const query = `
        SELECT * FROM posts
        WHERE postid = ${id};
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

const insertPost = async (user: any) => {
    console.log(user)
    const query = `
        INSERT INTO posts (postid, comments, title, content, post_date, username)
	        VALUES
		        ($1, $2, $3, $4, $5, $6);
    `
    const params = [...Object.values(user)]    
    console.log(`Inserting a new post ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New post inserted succesfully.`)
    return result
}

const deletePost = async (id: Number) => {
    const query=`
        DELETE FROM posts
        WHERE postid = ${id};
    `
    //Works, but crashes if the post still has comments on it left
    //So this should check if this post still has comments and delete those first
    console.log(query)
    const result = await executeQuery(query)
    return result
}

const updatePost = async (id: number, content: string) => {
    const query =`
    UPDATE posts
    SET content = '${content}'
    WHERE postid = ${id};
    `
    console.log(query)
    const result = await executeQuery(query)
    return result
}

////////////////


/////////////////
// COMMENTS

const getComments = async (username: string) => {
    const query = `
        SELECT * FROM comments
        WHERE username = '${username}';
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

const insertComment = async (user: any) => {
    console.log(user)
    const query = `
        INSERT INTO comments (commentid, username, content, comment_date, postid)
	        VALUES
		        ($1, $2, $3, $4, $5);
    `
    const params = [...Object.values(user)]    
    console.log(`Inserting a new comment ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New comment inserted succesfully.`)
    return result
}

const deleteComment = async (id: Number) => {
    const query=`
        DELETE FROM comments
        WHERE commentid = ${id};
    `
    console.log(query)
    const result = await executeQuery(query)
    return result
}

const updateComment = async (id: number, content: string) => {
    const query =`
    UPDATE comments
    SET content = '${content}'
    WHERE commentid = ${id};
    `
    console.log(query)
    const result = await executeQuery(query)
    return result
}

////////////////
// const updateUser = async (id: number, user:any) => {
//     console.log(id, user)
//     const query = `
//         UPDATE users
// 	    SET name = $1, price = $2
// 		WHERE userid = ${id};
//     `
//     const params = [...Object.values(user)]
//     console.log(`Inserting a new user ${params}...`)
//     const result = await executeQuery(query, params)
//     console.log(`New user updated succesfully.`)
//     return result
// }

// const deleteUser = async (id: number) => {
//     console.log(id)
//     const query = `
//         DELETE FROM users WHERE id = ${id};
//     `
//     const result = await executeQuery(query)
//     console.log(`${id} was deleted from users`)
//     return result
// }

export default { findAllUsers, insertUser, getUser, deleteUser, updateUser,
    findAllPosts, getPost, insertPost,deletePost, updatePost,
    getComments, insertComment, deleteComment, updateComment }