import express from 'express'
import productsRouter from "./productsRouter"

const server = express()
server.use(express.json())
server.use("/", productsRouter)

const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})