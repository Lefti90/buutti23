import { Router } from "express"
import dao from "./productDao"

const router = Router()

router.post("/", async (req, res) =>{
    const product = req.body
    const result = await dao.insertProduct(product)
    const storedProduct = {id: result.rows[0], ...product}
    res.send(storedProduct)
})

router.get("/", async (_req, res) => {
    const result = await dao.findAll()
    res.send(result)
})

router.post("/:id", async (req, res) => {
    const product = req.body
    const id = Number(req.params.id)
    const _result = await dao.updateProduct(id, product)
    const storedProduct = {product}
    res.send(storedProduct)
})

router.delete("/:id", async (req, res) => {
    const id = Number(req.params.id)
    const _result = await dao.deleteProduct(id)
    res.send(`${id} was deleted from products`)
})

router.get("/:id", async (req, res) => {
    const id = Number(req.params.id)
    const result = await dao.getProduct(id)
    res.send(result.rows)
})

export default router