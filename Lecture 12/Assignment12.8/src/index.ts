import express from 'express'
import productsRouter from "./productsRouter"
import { createProductsTable } from "./db"

const server = express()
server.use(express.json())

createProductsTable()

server.use("/", productsRouter)

const PORT= Number(process.env.PORT)
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})