import { executeQuery } from "./db"

const insertProduct = async (product: any) => {
    console.log(product)
    const query = `
        INSERT INTO products (name, price)
	        VALUES
		        ($1, $2);
    `
    const params = [...Object.values(product)]    
    console.log(`Inserting a new product ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New product inserted succesfully.`)
    return result
}

const findAll = async () => {
    const query = `SELECT * FROM products`
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result.rows
}

const updateProduct = async (id: number, product:any) => {
    console.log(id, product)
    const query = `
        UPDATE products
	    SET name = $1, price = $2
		WHERE id = ${id};
    `
    const params = [...Object.values(product)]
    console.log(`Inserting a new product ${params}...`)
    const result = await executeQuery(query, params)
    console.log(`New product updated succesfully.`)
    return result
}

const deleteProduct = async (id: number) => {
    console.log(id)
    const query = `
        DELETE FROM products WHERE id = ${id};
    `
    const result = await executeQuery(query)
    console.log(`${id} was deleted from products`)
    return result
}

const getProduct = async (id:number) => {
    const query = `
        SELECT * FROM products
        WHERE id = ${id};
    `
    const result = await executeQuery(query)
    console.log("Result: ", result.rows)
    return result
}

export default { findAll, insertProduct, updateProduct, deleteProduct, getProduct }