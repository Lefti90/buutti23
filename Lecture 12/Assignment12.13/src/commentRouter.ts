import { Router } from "express"
import dao from "./dao"

const router = Router()

router.post("/", async (req, res) =>{
    const user = req.body
    const result = await dao.insertComment(user)
    const storedUser = {id: result.rows[0], ...user}
    res.send(storedUser)
})

export default router