import { Router } from "express"
import dao from "./dao"

const router = Router()

router.get("/:username", async (req, res) => {
    const username = req.params.username
    console.log(username)
    const result = await dao.getComments(username)
    res.send(result.rows)
})

export default router