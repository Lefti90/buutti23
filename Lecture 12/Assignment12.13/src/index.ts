import express from 'express'
import usersRouter from "./usersRouter"
import postsRouter from "./postsRouter"
import commentsRouter from "./commentsRouter"
import commentRouter from "./commentRouter"

const server = express()
server.use(express.json())
server.use("/users", usersRouter)
server.use("/posts", postsRouter)
server.use("/comments", commentsRouter)
server.use("/comment", commentRouter)


const { PORT } = process.env
server.listen(PORT, () => {
    console.log('Products API listening to port', PORT)
})