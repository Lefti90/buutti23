"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const middleware_1 = require("./middleware");
const router = express_1.default.Router();
const notices = [
    {
        "id": 0,
        "content": "Ostakaa makkaraa!"
    },
    {
        "id": 1,
        "content": "Syökää kanaa!"
    }
];
router.get("/", (req, res) => {
    console.log("GET");
    console.log(req.body);
    res.status(200).send(notices);
});
router.post("/", middleware_1.validateNotice, (req, res) => {
    console.log("POST");
    const body = req.body;
    notices.push(body);
    res.sendStatus(201);
});
router.delete("/:id", (req, res) => {
    console.log("DELETE");
    const id = Number(req.params.id);
    const notice = notices.find(item => item.id === id);
    console.log(id, notice);
    if (notice === undefined) {
        return res.status(404).send("Error 404, invalid book id");
    }
    else {
        notices.splice(notices[id].id, 1);
    }
    res.sendStatus(200);
});
exports.default = router;
