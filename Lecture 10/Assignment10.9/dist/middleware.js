"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateNotice = void 0;
const validateNotice = (req, res, next) => {
    const { id, content } = req.body;
    if (typeof (id) !== "number" || typeof (content) !== "string") {
        return res.status(400).send("Missing or invalid parameters");
    }
    next();
};
exports.validateNotice = validateNotice;
const unkownEndpoint = (_req, res) => {
    console.log("ERROR 404");
    res.status(404).send("Error 404");
};
exports.default = unkownEndpoint;
