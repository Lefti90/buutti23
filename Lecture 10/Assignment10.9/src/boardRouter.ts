import express, { Request, Response } from "express"
import { validateNotice } from "./middleware"

const router = express.Router()

interface Notice{
    id: number
    content: string
}

const notices: Array<Notice> = [
    {
        "id": 0,
        "content": "Ostakaa makkaraa!"
    },
    {
        "id": 1,
        "content": "Syökää kanaa!"
    }
]

router.get("/", (req: Request, res: Response) => {
    console.log("GET")
    console.log(req.body)
    res.status(200).send(notices)
})

router.post("/", validateNotice,(req: Request, res: Response) => {
    console.log("POST")
    const body: Notice = req.body
    notices.push(body)
    res.sendStatus(201)
})

router.delete("/:id", (req: Request, res: Response) => {
    console.log("DELETE")
    const id = Number(req.params.id)
    const notice = notices.find(item => item.id === id)
    console.log(id, notice)
    if (notice === undefined) {
        return res.status(404).send("Error 404, invalid book id")
    }else{
        notices.splice(notices[id].id, 1)
    }
    res.sendStatus(200)    
})







export default router