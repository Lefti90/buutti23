import express from "express"
import boardRouter from "./boardRouter"
import unkownEndpoint from "./middleware"

const server = express()
//Body parser for json undefined problem
server.use(express.json())
server.use("/board", boardRouter)

server.use(unkownEndpoint)
const port = 3001
server.listen(port, () => {
    console.log("Server listening port", port)
})