import { Request, Response, NextFunction } from "express"

const validateNotice = (req: Request, res: Response, next: NextFunction ) => {
    const { id, content } = req.body
    if (typeof(id) !== "number" || typeof(content) !== "string") {
        return res.status(400).send("Missing or invalid parameters")
    }
    next()
}

const unkownEndpoint = (_req: Request, res: Response) => {
    console.log("ERROR 404")
    res.status(404).send("Error 404")
}

export default unkownEndpoint
export { validateNotice }