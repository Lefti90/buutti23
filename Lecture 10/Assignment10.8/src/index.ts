let startNum = 0
let lastNum = 1
let firstNum = 0

function delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) )
}

async function printFibonacci(){
    do {
        const curNum = lastNum + firstNum
        await delay(1000)
        console.log(curNum)
        firstNum = lastNum
        lastNum = curNum
    } while (lastNum < 55);
}

printFibonacci()