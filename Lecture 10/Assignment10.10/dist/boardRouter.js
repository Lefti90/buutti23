"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const middleware_1 = require("./middleware");
const argon2_1 = __importDefault(require("argon2"));
require("dotenv/config");
const router = express_1.default.Router();
let loggedIn = false;
const users = [
    {
        "username": "admin",
        "password": (_a = process.env.BOARD_ADMIN_PASSWORD) !== null && _a !== void 0 ? _a : ""
    }
];
const notices = [
    {
        "id": 0,
        "content": "Ostakaa makkaraa!"
    },
    {
        "id": 1,
        "content": "Syökää kanaa!"
    }
];
router.get("/", middleware_1.authenticate, (req, res) => {
    console.log("GET");
    if (!loggedIn) {
        return res.status(400).send("You need to be logged in!");
    }
    res.status(200).send(notices);
});
router.get("/secretnote", middleware_1.authenticate, (req, res) => {
    console.log("GET");
    if (!loggedIn) {
        return res.status(400).send("You need to be logged in!");
    }
    res.status(200).send(process.env.BOARD_SECRETNOTE);
});
router.post("/", middleware_1.validateNotice, middleware_1.authenticate, (req, res) => {
    console.log("POST");
    if (!loggedIn) {
        return res.status(400).send("You need to be logged in!");
    }
    const body = req.body;
    notices.push(body);
    res.sendStatus(201);
});
router.delete("/:id", middleware_1.authenticate, (req, res) => {
    console.log("DELETE");
    if (!loggedIn) {
        return res.status(400).send("You need to be logged in!");
    }
    const id = Number(req.params.id);
    const notice = notices.find(item => item.id === id);
    console.log(id, notice);
    if (notice === undefined) {
        return res.status(404).send("Error 404, invalid book id");
    }
    else {
        notices.splice(notices[id].id, 1);
    }
    res.sendStatus(200);
});
//User
router.post("/login", middleware_1.authenticate, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { username, password } = req.body;
    console.log(username, password);
    const user = users.find(user => user.username === req.body.username);
    if (user === undefined) {
        return res.status(401).send("Invalid username or password");
    }
    const hash = user.password;
    const pw = req.body.password;
    const isCorrectPassword = yield argon2_1.default.verify(hash, pw);
    if (!isCorrectPassword) {
        return res.status(401).send("Invalid username or password");
    }
    console.log("LOGGED IN");
    loggedIn = true;
    res.status(200).send("Logged in");
}));
exports.default = router;
