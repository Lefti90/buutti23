"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticate = exports.validateNotice = void 0;
require("dotenv/config");
const validateNotice = (req, res, next) => {
    const { id, content } = req.body;
    if (typeof (id) !== "number" || typeof (content) !== "string") {
        return res.status(400).send("Missing or invalid parameters");
    }
    next();
};
exports.validateNotice = validateNotice;
const authenticate = (req, res, next) => {
    var _a;
    const auth = req.get("Authorization");
    if (!(auth === null || auth === void 0 ? void 0 : auth.startsWith("Bearer "))) {
        return res.status(401).send("Invalid token or privileges");
    }
    const token = auth.substring(7);
    const secret = (_a = process.env.BOARD_ADMIN_TOKEN) !== null && _a !== void 0 ? _a : "";
    try {
        //const decodedToken = jwt.verify(token, secret)
        //console.log("DECODED TOKEN", decodedToken)
        console.log("SECRET:", secret);
        //req.user = decodedToken
        if (token === process.env.BOARD_ADMIN_TOKEN) {
            next();
        }
        else {
            console.log("Error: Invalid token or privileges");
            res.status(400).send("Invalid token or privileges");
        }
    }
    catch (error) {
        console.log("SECRET:", secret);
        console.log("REQ", req.body, auth, token);
        return res.status(401).send("Invalid token or privileges");
    }
};
exports.authenticate = authenticate;
const unkownEndpoint = (_req, res) => {
    console.log("ERROR 404");
    res.status(404).send("Error 404");
};
exports.default = unkownEndpoint;
