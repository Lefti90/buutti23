"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const boardRouter_1 = __importDefault(require("./boardRouter"));
const middleware_1 = __importDefault(require("./middleware"));
const server = (0, express_1.default)();
//Body parser for json undefined problem
server.use(express_1.default.json());
server.use("/board", boardRouter_1.default);
server.use(middleware_1.default);
const port = process.env.ENV_PORT;
server.listen(port, () => {
    console.log("Server listening port", port);
});
