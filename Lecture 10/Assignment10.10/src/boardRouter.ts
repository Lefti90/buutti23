import express, { Request, Response } from "express"
import { authenticate, validateNotice } from "./middleware"
import argon2 from "argon2"
import "dotenv/config"

const router = express.Router()

interface Notice{
    id: number
    content: string
}

//Maybe should make a different router for users
//But only few lines so its not bad, right... right?
interface User{
    username: string
    password: string
}
let loggedIn: boolean = false

const users: Array<User> = [
    {
        "username": "admin",
        "password": process.env.BOARD_ADMIN_PASSWORD ?? ""
    }
]

const notices: Array<Notice> = [
    {
        "id": 0,
        "content": "Ostakaa makkaraa!"
    },
    {
        "id": 1,
        "content": "Syökää kanaa!"
    }
]

router.get("/", authenticate,(req: Request, res: Response) => {
    console.log("GET")
    if(!loggedIn){
        return res.status(400).send("You need to be logged in!")
    }
    res.status(200).send(notices)
})

router.get("/secretnote", authenticate,(req: Request, res: Response) => {
    console.log("GET")
    if(!loggedIn){
        return res.status(400).send("You need to be logged in!")
    }
    res.status(200).send(process.env.BOARD_SECRETNOTE)
})

router.post("/", validateNotice, authenticate,(req: Request, res: Response) => {
    console.log("POST")
    if(!loggedIn){
        return res.status(400).send("You need to be logged in!")
    }
    const body: Notice = req.body
    notices.push(body)
    res.sendStatus(201)
})

router.delete("/:id", authenticate,(req: Request, res: Response) => {
    console.log("DELETE")
    if(!loggedIn){
        return res.status(400).send("You need to be logged in!")
    }
    const id = Number(req.params.id)
    const notice = notices.find(item => item.id === id)
    console.log(id, notice)
    if (notice === undefined) {
        return res.status(404).send("Error 404, invalid book id")
    }else{
        notices.splice(notices[id].id, 1)
    }
    res.sendStatus(200)    
})


//User
router.post("/login", authenticate, async(req: Request, res: Response) => {

    const { username, password } = req.body
    console.log(username, password)
    const user = users.find(user => user.username === req.body.username)
    if(user === undefined){
        return res.status(401).send("Invalid username or password")
    }    
    const hash = user.password
    const pw = req.body.password
    const isCorrectPassword = await argon2.verify(hash, pw)
    if(!isCorrectPassword){
        return res.status(401).send("Invalid username or password")
    }

    console.log("LOGGED IN")
    loggedIn = true
    res.status(200).send("Logged in")
})







export default router