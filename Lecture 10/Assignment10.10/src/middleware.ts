import { Request, Response, NextFunction } from "express"
import "dotenv/config"

const validateNotice = (req: Request, res: Response, next: NextFunction ) => {
    const { id, content } = req.body
    if (typeof(id) !== "number" || typeof(content) !== "string") {
        return res.status(400).send("Missing or invalid parameters")
    }
    next()
}

const authenticate = (req: Request, res: Response, next: NextFunction ) => {
    const auth = req.get("Authorization")
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Invalid token or privileges")
    }
    const token = auth.substring(7)
    const secret = process.env.BOARD_ADMIN_TOKEN ?? ""
    try {
        //const decodedToken = jwt.verify(token, secret)
        //console.log("DECODED TOKEN", decodedToken)
        console.log("SECRET:", secret)
        //req.user = decodedToken
        if(token === process.env.BOARD_ADMIN_TOKEN){
            next()
        }else{
            console.log("Error: Invalid token or privileges")
            res.status(400).send("Invalid token or privileges")
        }
        
    } catch (error) {
        console.log("SECRET:", secret)
        console.log("REQ", req.body, auth, token)
        return res.status(401).send("Invalid token or privileges")
    }
}

const unkownEndpoint = (_req: Request, res: Response) => {
    console.log("ERROR 404")
    res.status(404).send("Error 404")
}

export default unkownEndpoint
export { validateNotice, authenticate }