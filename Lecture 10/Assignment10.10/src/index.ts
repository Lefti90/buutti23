import express from "express"
import boardRouter from "./boardRouter"
import unkownEndpoint from "./middleware"
import "dotenv/config"

const server = express()
//Body parser for json undefined problem
server.use(express.json())
server.use("/board", boardRouter)

server.use(unkownEndpoint)
const port = process.env.ENV_PORT
server.listen(port, () => {
    console.log("Server listening port", port)
})