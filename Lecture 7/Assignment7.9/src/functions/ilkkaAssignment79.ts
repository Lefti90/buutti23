import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";



export async function ilkkaAssignment79(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    const min = request.query.get('min') || await request.text() || 'min';
    const max = request.query.get('max') || await request.text() || 'max';
    const isInt = request.query.get('isInt') || await request.text() || 'bool';

    const minNum = parseInt(min)
    const maxNum = parseInt(max)

    const randomNumber = Math.floor(Math.random() * (maxNum - minNum) + minNum)


    return { body: `Random number between ${min} and ${max} is: ${randomNumber}!` };
};

app.http('ilkkaAssignment79', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: ilkkaAssignment79
});
