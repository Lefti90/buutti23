"use strict";
// Write tests for multiplication. 
// Remember to include the “difficult”, 
// and borderline cases.
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../index");
test("dummy test", () => {
    expect(true).toBe(true);
});
test("multiplication test", () => {
    const answer = (0, index_1.calculator)("*", 2, 4);
    expect(answer).toBe(8);
});
