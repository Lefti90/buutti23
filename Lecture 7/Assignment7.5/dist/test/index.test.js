"use strict";
// Write tests for multiplication. 
// Remember to include the “difficult”, 
// and borderline cases.
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../index");
test("dummy test", () => {
    expect(true).toBe(true);
});
//Multiplications
test("multiplication test 2 * 4", () => {
    const answer = (0, index_1.calculator)("*", 2, 4);
    expect(answer).toBe(8);
});
test("multiplication 0 * 4", () => {
    const answer = (0, index_1.calculator)("*", 0, 4);
    expect(answer).toBe(0);
});
test("multiplication test -2 * 4", () => {
    const answer = (0, index_1.calculator)("*", -2, 4);
    expect(answer).toBe(-8);
});
test("multiplication test pi * e", () => {
    const pi = Math.PI;
    const e = Math.E;
    const answer = (0, index_1.calculator)("*", pi, e);
    expect(answer).toBe(pi * e);
});
//Divisions
describe("Division tests", () => {
    it("Division 4 / 2", () => {
        const answer = (0, index_1.calculator)("/", 4, 2);
        expect(answer).toBe(2);
    });
    it("Division 4 / 0", () => {
        const answer = (0, index_1.calculator)("/", 4, 0);
        expect(answer).toBe(Infinity);
    });
    it("Division 4 / -2", () => {
        const answer = (0, index_1.calculator)("/", 4, -2);
        expect(answer).toBe(-2);
    });
});
//Sums
describe("Sum tests", () => {
    it("Sum 4 + 2", () => {
        const answer = (0, index_1.calculator)("+", 4, 2);
        expect(answer).toBe(6);
    });
    it("Sum 4 + Infinity", () => {
        const answer = (0, index_1.calculator)("+", 4, Infinity);
        expect(answer).toBe(Infinity);
    });
    it("Division 4 + -2", () => {
        const answer = (0, index_1.calculator)("+", 4, -2);
        expect(answer).toBe(2);
    });
});
//Diff
describe("Diff tests", () => {
    it("Sum 4 + 2", () => {
        const answer = (0, index_1.calculator)("-", 4, 2);
        expect(answer).toBe(2);
    });
    it("Diff 4 + Infinity", () => {
        const answer = (0, index_1.calculator)("-", 4, Infinity);
        expect(answer).toBe(-Infinity);
    });
    it("Diff 4 + -2", () => {
        const answer = (0, index_1.calculator)("-", 4, -2);
        expect(answer).toBe(6);
    });
});
