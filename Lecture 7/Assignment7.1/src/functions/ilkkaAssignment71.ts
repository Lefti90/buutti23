import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

export async function ilkkaAssignment71(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    const input = request.query.get('input') || await request.text() || 'empty';

    return { body: `Your input was: ${input.toUpperCase()}!` };
};

app.http('ilkkaAssignment71', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: ilkkaAssignment71
});
