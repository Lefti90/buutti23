"use strict";
// Assignment 7.7: Command Line Converter
Object.defineProperty(exports, "__esModule", { value: true });
exports.converter = void 0;
// Write a command line unit converter that converts between volume units. 
// Your converter should accept at least units deciliter, liter, ounce, cup, 
// and pint.
// The program takes three parameters: amount, source unit, and the target unit.
// For example convert 6 dl oz --> 20
// Write tests for your converter. Include at least five tests!
const input = process.argv[2];
const input1 = process.argv[3];
const input2 = process.argv[4];
converter(parseFloat(input), input1, input2);
function converter(amount, sourceUnit, targetUnit) {
    //turn everything in to liters
    let inLiters;
    let inTarget;
    switch (sourceUnit) {
        case "dl":
            inLiters = amount * 0.1;
            break;
        case "l":
            inLiters = amount * 1;
            break;
        case "ounce":
            inLiters = amount * 0.0295735;
            break;
        case "cup":
            inLiters = amount * 0.236588;
            break;
        case "pint":
            inLiters = amount * 0.473176;
            break;
        default:
            console.log(sourceUnit, "was not in the list");
            break;
    }
    if (inLiters != null) {
        switch (targetUnit) {
            //litrat kertaa 10 on Y desilitraa
            case "dl":
                inTarget = Math.round((inLiters * 10) * 1000) / 1000;
                break;
            case "l":
                inTarget = Math.round((inLiters * 1) * 1000) / 1000;
                break;
            case "ounce":
                inTarget = Math.round((inLiters * 33.814) * 1000) / 1000;
                break;
            case "cup":
                inTarget = Math.round((inLiters * 4.227) * 1000) / 1000;
                break;
            case "pint":
                inTarget = Math.round((inLiters * 2.113) * 1000) / 1000;
                break;
            default:
                console.log(targetUnit, "was not in the list");
                break;
        }
    }
    else {
        console.log(targetUnit, "was not in the list");
    }
    console.log(inTarget);
    return inTarget;
}
exports.converter = converter;
