"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../index");
test("dummy test", () => {
    expect(true).toBe(true);
});
//Divisions
describe("Conversion test", () => {
    it("1 deciliters to liters", () => {
        const answer = (0, index_1.converter)(1, "dl", "l");
        expect(answer).toBe(0.1);
    });
    it("2 cups to pints", () => {
        const answer = (0, index_1.converter)(2, "cup", "pint");
        expect(answer).toBe(1);
    });
    it("3 ounces to cups", () => {
        const answer = (0, index_1.converter)(3, "ounce", "cup");
        expect(answer).toBe(0.375);
    });
    it("4 cups to deciliters", () => {
        const answer = (0, index_1.converter)(4, "cup", "dl");
        expect(answer).toBe(9.464);
    });
    it("5 liters to liters", () => {
        const answer = (0, index_1.converter)(5, "l", "l");
        expect(answer).toBe(5);
    });
});
