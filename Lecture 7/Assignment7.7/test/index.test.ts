import { converter } from "../index"

test("dummy test", () =>{
    expect(true).toBe(true)
})

//Divisions
describe("Conversion test", () => {
    it("1 deciliters to liters", () =>{
        const answer = converter(1, "dl", "l")
        expect(answer).toBe(0.1)
    })

    it("2 cups to pints", () => {
        const answer = converter(2, "cup", "pint")
        expect(answer).toBe(1)
    })

    it("3 ounces to cups", () => {
        const answer = converter(3, "ounce", "cup")
        expect(answer).toBe(0.375)
    })

    it("4 cups to deciliters", () => {
        const answer = converter(4, "cup", "dl")
        expect(answer).toBe(9.464)
    })

    it("5 pints to liters", () => {
        const answer = converter(5, "pint", "l")
        expect(answer).toBe(2.366)
    })
})