export function calculator(operator: "+" | "-" | "*" | "/", num1: number, num2: number): number {
    switch (operator) {
    case "+": return num1 + num2
    case "-": return num1 - num2
    case "*": return num1 * num2
    case "/": return num1 / num2
    }
}