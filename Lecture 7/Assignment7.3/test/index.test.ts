// Write tests for multiplication. 
// Remember to include the “difficult”, 
// and borderline cases.

import { calculator } from "../index"

test("dummy test", () =>{
    expect(true).toBe(true)
})

test("multiplication test 2 * 4", () =>{
    const answer = calculator("*", 2, 4)
    expect(answer).toBe(8)
})

test("multiplication 0 * 4", () =>{
    const answer = calculator("*", 0, 4)
    expect(answer).toBe(0)
})

test("multiplication test -2 * 4", () =>{
    const answer = calculator("*", -2, 4)
    expect(answer).toBe(-8)
})

test("multiplication test pi * e", () =>{
    const pi = Math.PI
    const e = Math.E
    const answer = calculator("*", pi, e)
    expect(answer).toBe(pi * e)
})

test("fail this test", () =>{
    expect(true).toBe(false)
})