import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

let counter = 0
export async function ilkkaAssignment710(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    //const dog = request.query.get("https://dog.ceo/api/breeds/image/random")

    //const dog = request.query.get("https://dog.ceo/api/breeds/image/random") || await request.json() || "fug:D"

    const catApiResponse = await fetch("https://cataas.com/cat");
    const catData = await catApiResponse.json();
    const message = catData.message
    const catImageUrl = catData.message;

    const name = request.query.get('name') || await request.text() || 'world';

    counter++

    if(counter % 13 == 0){
        //return fox
    }else{
        //return cat or dog
        const randomNumber = Math.random()
        if(randomNumber <= 0.5){
            //dog
        }else{
            //cat
        }
    }
    return { body: {
        message: "Random cat image", //kesken ei toimi :(
        imageUrl: catImageUrl
    },
    headers: {
        'Content-Type': 'application/json'
    } };
};

app.http('ilkkaAssignment710', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: ilkkaAssignment710
});
