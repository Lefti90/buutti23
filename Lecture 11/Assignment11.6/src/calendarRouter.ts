import express, { Request, Response } from "express"
import { validateEvent } from "./middleware"

const router = express.Router()

interface Event{
    id: number
    title: string
    description: string
    date: Date
    time: string | undefined
}

const events: Array<Event> = [
    {
        "id": 0,
        "title": "Pääsiäinen",
        "description": "Vapaata Jeesustelua",
        "date": new Date("2024, 03, 29"),
        "time": undefined
    },
    {
        "id": 1,
        "title": "Pääsiäinen 2",
        "description": "Vapaata Jeesustelua 2",
        "date": new Date("2024, 03, 30"),
        "time": undefined
    },
    {
        "id": 2,
        "title": "Jouluaatto",
        "description": "Jeesuksen synttärien etkot",
        "date": new Date("2024, 12, 24"),
        "time": undefined
    },
    {
        "id": 3,
        "title": "Joulupäivä",
        "description": "Jeesuksen synttärit",
        "date": new Date("2024, 12, 25"),
        "time": undefined
    }
]


router.get("/", (_req: Request, res: Response) => {
    console.log("GET")
    const now = new Date("March 29, 2024 00:00:00")
    console.log(now)
    res.status(200).send(events)
})

router.get("/:monthNumber", (req: Request, res: Response) => {
    console.log("GET /:monthNumber")
    const monthNumber = Number(req.params.monthNumber)
    //Get events happened in :monthNumber
    const monthsEvents: Array<Event> = []
    events.map(event => {
        if(event.date.getMonth() === monthNumber-1)
        {
            //add this to monthsEvents
            monthsEvents.push(event)
        }
    })
    res.status(200).send(monthsEvents)
})

router.post("/", validateEvent, (req: Request, res: Response) => {
    console.log("POST /")
    const newEvent: Event = req.body
    if(newEvent.id == undefined || newEvent.id === null){
        newEvent.id = events.length
    }    

    events.push(newEvent)
    res.status(200).send(newEvent)
})

router.put("/:id",validateEvent,(req: Request, res: Response) => {
    console.log("GET /:id")
    const body : Event = req.body
    const id = Number(req.params.id)
    const event = events.find(item => item.id === id)
    if(event === undefined){
        return res.status(404).send("Error 404, invalid event id")
    }else{
        event.title = body.title
        event.description = body.description
        event.date = body.date
    }

    res.status(200).send(event)
})

router.delete("/:id", (req: Request, res: Response) => {
    console.log("DELETE")

    const id = Number(req.params.id)
    const event = events.find(item => item.id === id)
    console.log(id, event)
    if (event === undefined) {
        return res.status(404).send("Error 404, invalid event id")
    }else{
        events.splice(events[id].id, 1)
    }
    res.sendStatus(200)    
})

export default router