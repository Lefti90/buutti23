import express from "express"
import unkownEndpoint from "./middleware"
import calendarRouter from "./calendarRouter"

const server = express()
server.use(express.json())
server.use("/calendar", calendarRouter)

server.use(unkownEndpoint)
const PORT = 3000
server.listen(PORT, () => {
    console.log("Listerning port: ", PORT)
})