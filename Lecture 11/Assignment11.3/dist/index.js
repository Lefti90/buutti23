"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const server = (0, express_1.default)();
server.get("/", (_req, res) => {
    res.send("OK");
});
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log("Server listening port", PORT);
});
