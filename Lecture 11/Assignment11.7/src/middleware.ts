import { Request, Response, NextFunction } from "express"

const unkownEndpoint = (_req: Request, res: Response) => {
    console.log("ERROR 404")
    res.status(404).send("Error 404")
}

const validateEvent = (req: Request, res: Response, next: NextFunction ) => {
    const { title, description, date } = req.body
    if (typeof(title) !== "string" || typeof(description) !== "string" || typeof(date) !== "string") {
        return res.status(400).send("Missing or invalid parameters")
    }

    next()
}

export default unkownEndpoint
export { validateEvent }