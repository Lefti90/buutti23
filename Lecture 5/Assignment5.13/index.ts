// Assignment 5.13: Likes

// Create a Post class that has properties author (string), content (string), and likers (list of strings). 
// The author is the name of the post author and content is the content of the post. 
// The likers is an array of users that have liked the post.

// The Post class shuld have a method like(user: string) that adds the user to the likers list. 
// The same user can not be added twice! It should also have a method likes that returns 
// one of the following strings depending on the length of the likes list:

//     0 likes: "No one likes this"
//     1 like: "[user] likes this"
//     2 likes: "[user1] and [user2] like this"
//     3 likes: "[user1], [user2] and [user3] like this
//     4 likes: "[user1], [user2] and 2 others like this"
//     5 or more likes: just as with 4 likes, but the number of "others" simply increases

class Post {
    author: string
    content: string
    likers: Array<string>

    constructor(author: string, content: string, likers: Array<string>){
        this.author = author
        this.content = content
        this.likers = likers
    }

    like(user: string){
        if(this.likers.find((user) => user) != user)
            this.likers.push(user)
        else
            return false
    }

    likes(){
        let likeText = ""
        let likeAmount = 0
        if(this.likers.length == 0){
            likeText = "No one likes this"
        }else if(this.likers.length == 1){
            likeText = this.likers[0] + " likes this"
        }else if(this.likers.length == 2){
            likeText = `${this.likers[0]} and ${this.likers[1]} likes this`
        }else if(this.likers.length == 3){
            likeText = `${this.likers[0]}, ${this.likers[1]} and ${this.likers[2]} likes this`
        }else{
            for (let i = 0; i < this.likers.length; i++) {
                likeAmount++                
            }
            likeText = `${this.likers[0]}, ${this.likers[1]} and ${likeAmount-2} others likes this`
        }

        return likeText
    }
}

const post1 = new Post("John", "Moi some!", [])
post1.like("Pekka")


console.log(post1)
console.log(post1.likes())