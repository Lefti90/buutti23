// Assignment 5.6: Extended Recipes
// Create a HotRecipe class that extends the Recipe you have already created. 
// HotRecipe should have a heatLevel parameter in addition to the Recipe parameters
// Override the existing toString method with a new one that adds a warning to the recipe
//  if the heatLevel is over 5.

class Ingredient {
    name: string
    amount: number

    constructor(name: string, amount: number){
        this.name = name
        this.amount = amount
    }
}

class Recipe {
    name: string
    ingredients: Ingredient[]
    servings: number

    constructor(name: string, ingredients: Array<Ingredient>, servings: number){
        this.name = name
        this.ingredients = ingredients
        this.servings = servings
    }
}

class HotRecipe extends Recipe {
    heatLevel: number
    constructor(name: string, ingredients: Array<Ingredient>, heatLevel: number){
        super(name, ingredients, heatLevel)
        this.heatLevel = 0
    }
}

const sugar = new Ingredient("sugar", 1)

const flour = new Ingredient("flour", 1)

const water = new Ingredient("water", 1)

const egg = new Ingredient("egg", 1)

const dough: Recipe = {
    name: "dough",
    ingredients: [sugar, flour, water, egg],
    servings: 1
}

const fireDough: HotRecipe = {
    name: "fire dough",
    ingredients: [sugar, flour, water, egg],
    servings: 2,
    heatLevel: 5
}

console.log(dough)
console.log(fireDough, fireDough.heatLevel)
