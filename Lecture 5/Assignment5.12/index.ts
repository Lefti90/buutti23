// Assignment 5.12: Banking

// Create classes Bank and Account.

// The Account class should have properties owner (string), id (string) and balance 
// (number), and methods deposit(amount: number) and withdraw(amount: number). 
// The deposit method should add the given amount to the balance. 
// The amount must be a positive number. The method should return true on success and false on failure.

// The Bank class should have properties name (string), accounts (list of Account objects) 
// and following methods:

//     openAcount(owner:string)
//     closeAccount(id: string)
//     deposit(accountId: string, amount: number)
//     withdraw(accountId: string, amount: number)
//     checkBalance(accountId: string)
//     transfer(fromAccountId: string, toAccountId: string, amount: number)
//     customers()
//     totalValue()

// The openAccount method should create a new account with a unique id and zero balance, 
// add it to the list of accounts, and return the created account id. The closeAccount 
// method should remove the account corresponding to the given id from the list of accounts, 
// and return the removed Account object. It should return null on failure.

// The deposit method should add the given amount to the bank account corresponding to the
//  given account id. It should return true on success and false on failure. The withdraw 
//  method should remove the given amount from the account corresponding to the given account id. 
//  It should return the removed sum, or null on failure.

// The checkBalance should return the balance of the account corresponding to the given id, 
// or null on failure. The transfer method should move money from one account to another. 
// It should return true on success and false on failure. Note that if either the removal 
// or the addition fails for any reason, the other operation should also not happen.

// The customers method returns the list of all account owners, and the totalValue method 
// returns the sum of all account balances.

// Test your code by creating a bank and some customers and using all the methods, checking 
// that the results are what they should be.

class Bank {
    name: string
    accounts: Account[]

    constructor(name: string, accounts: Array<Account>){
        this.name = name
        this.accounts = accounts
    }

    openAccount(owner: string){
        const newAcc = new Account(owner, "XYZ"+this.accounts.length, 0)
        this.accounts.push(newAcc)
        return newAcc.id
    }

    closeAccount(id: string){
        for (let i = 0; i < this.accounts.length; i++) {
            if(this.accounts[i].id == id){
                const accountToRemove = this.accounts[i]
                this.accounts.splice(i, 1)
                return accountToRemove
            }
        }
    }

    deposit(accountId: string, amount: number){
        for (let i = 0; i < this.accounts.length; i++) {
            if(this.accounts[i].id == accountId){
                this.accounts[i].deposit(amount)
                return true
            }         
        }
        return null
    }

    withdraw(accountId: string, amount: number){
        for (let i = 0; i < this.accounts.length; i++) {
            if(this.accounts[i].id == accountId){
                this.accounts[i].withdraw(amount)
                return amount
            }         
        }
        return null
    }

    checkBalance(accountId: string){
        for (let i = 0; i < this.accounts.length; i++) {
            if(this.accounts[i].id == accountId){
                const balance = this.accounts[i].balance
                console.log(balance)
                return balance
            }else{
                return null
            }            
        }
    }

    transfer(fromAccountId: string, toAccountId: string, amount: number){
        let senderId = ""
        let receiverId = ""

        for (let i = 0; i < this.accounts.length; i++) {
            if(this.accounts[i].id == fromAccountId){
                senderId = this.accounts[i].id               
            }
        }

        for (let i = 0; i < this.accounts.length; i++) {
            if(this.accounts[i].id == toAccountId){
                receiverId = this.accounts[i].id
            } 
        }

        if(senderId != "" && receiverId != ""){
            this.deposit(receiverId, amount)
            this.withdraw(senderId, amount)
            return true
        }else{
            return null
        }
    }

    customers(){
        const customers: Array<string> = []
        for (let i = 0; i < this.accounts.length; i++) {
            customers.push(this.accounts[i].owner )         
        }
        return customers
    }

    totalValue(){
        let sum = 0
        for (let i = 0; i < this.accounts.length; i++) {
            sum = sum + this.accounts[i].balance       
        }
        return sum
    }
}

class Account {
    owner: string
    id: string
    balance: number

    constructor(owner: string, id: string, balance: number){
        this.owner = owner
        this.id = id
        this.balance = balance
    }

    deposit (amount: number){
        this.balance = this.balance + amount
    }

    withdraw (amount: number){
        this.balance = this.balance - amount
    }
}

const sPankki = new Bank("S-Pankki", [])
sPankki.openAccount("John Doe")
sPankki.openAccount("Maija Meikalainen")

console.log(sPankki.deposit("XYZ0", 100))
console.log(sPankki.deposit("XYZ1", 100))
// console.log(sPankki.withdraw("XYZ0", 50))
// console.log(sPankki.withdraw("XYZ1", 50))
// console.log(sPankki.transfer("XYZ0", "XYZ1", 33))
// console.log(sPankki.checkBalance("XYZ0"))
// console.log(sPankki.customers())

console.log(sPankki.totalValue())

console.log(sPankki)