"use strict";
// Assignment 5.10: Point
// Create class Point that has two properties: x and y (numbers). 
// The coordinate class should have four methods: moveNorth, moveEast, moveSouth, and moveWest. 
// Each method should take one parameter: distance (number). 
// Using the method should move the point the given distance to the corresponding direction.
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    moveNorth(distance) {
        this.y = this.y + distance;
    }
    moveSouth(distance) {
        this.y = this.y - distance;
    }
    moveWest(distance) {
        this.x = this.x - distance;
    }
    moveEast(distance) {
        this.x = this.x + distance;
    }
}
const coords = new Point(0, 0);
coords.moveEast(5);
coords.moveNorth(5);
coords.moveSouth(4);
coords.moveWest(4);
console.log(coords);
