"use strict";
// Assignment 5.4: Methodical Recipes
// Add a metod "setServings" to your recipe. The method should take one parameter: servings (number). 
// The method should scale the recipe so that after the method has been used, 
// the recipe servings have been set to the 
// new number, and all the ingredient amounts have been scaled accordingly.
const sugar = {
    name: "sugar",
    amount: 1
};
const flour = {
    name: "flour",
    amount: 1
};
const water = {
    name: "water",
    amount: 1
};
const egg = {
    name: "egg",
    amount: 1
};
const dough = {
    name: "dough",
    ingredients: [sugar, flour, water, egg],
    servings: 2,
    setServings: function (multiplier) {
        this.servings = this.servings * multiplier;
        for (let i = 0; i < this.ingredients.length; i++) {
            this.ingredients[i].amount = this.ingredients[i].amount * multiplier;
        }
    }
};
dough.setServings(3);
console.log(dough);
