// Assignment 5.11: SpacePoint

// Create a class SpacePoint that extend the class from the previous assignment,
//  so that the new class can handle three-dimensional movement.

class Point {
    x: number
    y: number

    constructor(x: number, y: number){
        this.x = x
        this.y = y
    }

    moveNorth(distance: number){
        this.y = this.y+distance
    }

    moveSouth(distance: number){
        this.y = this.y-distance
    }

    moveWest(distance: number){
        this.x = this.x-distance
    }

    moveEast(distance: number){
        this.x = this.x + distance
    }
}

class SpacePoint extends Point {
    z: number
    constructor(x: number, y: number, z: number){
        super(x, y)
        this.z = z
    }

    moveForward(distance: number){
        this.z = this.z + distance
    }

    moveBack(distance: number){
        this.z = this.z - distance
    }
}


const coor3d = new SpacePoint(0,0,0)
coor3d.moveWest(1)
coor3d.moveForward(1.5)

console.log(coor3d)
