"use strict";
// Assignment 5.11: SpacePoint
// Create a class SpacePoint that extend the class from the previous assignment,
//  so that the new class can handle three-dimensional movement.
class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    moveNorth(distance) {
        this.y = this.y + distance;
    }
    moveSouth(distance) {
        this.y = this.y - distance;
    }
    moveWest(distance) {
        this.x = this.x - distance;
    }
    moveEast(distance) {
        this.x = this.x + distance;
    }
}
class SpacePoint extends Point {
    constructor(x, y, z) {
        super(x, y);
        this.z = z;
    }
    moveForward(distance) {
        this.z = this.z + distance;
    }
    moveBack(distance) {
        this.z = this.z - distance;
    }
}
const coor3d = new SpacePoint(0, 0, 0);
coor3d.moveWest(1);
coor3d.moveForward(1.5);
console.log(coor3d);
