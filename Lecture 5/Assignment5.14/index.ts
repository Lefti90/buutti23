// Assignment 5.14: Credential Generator

// Create a function (or multiple functions) that generates username and password from given 
// firstname and lastname.

// Username: B + last 2 numbers from current year + 2 first letters from both last name and 
// first name in lower case

// Password: 1 random letter + first letter of first name in lowercase + last letter of 
// last name in uppercase + random special character + last 2 numbers from current year

// Get to know to ASCII table. Random letters and special characters can be searched from 
// ASCII table with String.fromCharCode() method with indexes. For example String.fromCharCode(65) 
// returns letter A.

// Hints: Generate random numbers (indexes) to get one random letter and one special character.
//  Use range of 65 to 90 to get the (uppercase) LETTER and 33 to 47 to get the SPECIAL CHARACTER. 
//  Notice, that these are not the only "special characters", 
//  but using this range is acceptable for this exercise. Use build-in function to get the current year.

// Links: http://www.asciitable.com/

// generateCredentials('John', 'Doe') // [ 'B22dojo', 'KjE,22' ]

class User {
    firstName: string
    lastName: string

    constructor(firstName: string, lastName: string){
        this.firstName = firstName
        this.lastName = lastName
    }

    generateUsername(){
        const year = new Date().getFullYear().toString().slice(2,4)
        const username = `B${year}${this.lastName.slice(0,2)}${this.firstName.slice(0,2)}`
        console.log(username)
    }

    generatePassword(){
        const char1 = String.fromCharCode(Math.floor(Math.random() * (90 - 65) + 65))
        const char2 = this.firstName[0].toLowerCase()
        const char3 = this.lastName[this.lastName.length-1].toUpperCase()
        const char4 = String.fromCharCode(Math.floor(Math.random() * (47 - 33) + 33))
        const char5 = new Date().getFullYear().toString().slice(2,4)
        const password = `${char1}${char2}${char3}${char4}${char5}`
        console.log(password)
    }

    generateCredientals(){
        this.generateUsername()
        this.generatePassword()
    }
}

const pekka = new User("pekka","parantaja")
console.log(pekka)
pekka.generateUsername()
pekka.generatePassword()