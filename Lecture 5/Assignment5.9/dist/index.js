"use strict";
// Assignment 5.9: Forecast
// Create forecast_data.json and then add to following data to
// Then retrieve the data and modify the temperature of that forecast. 
// Lastly, save the change back to the file.
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const forecast = JSON.parse(fs_1.default.readFileSync("./forecast_data.json", "utf-8"));
forecast.temperature++;
fs_1.default.writeFileSync("./forecast_data.json", JSON.stringify(forecast), "utf-8");
console.log(forecast);
