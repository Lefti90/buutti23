// Assignment 5.9: Forecast
// Create forecast_data.json and then add to following data to
// Then retrieve the data and modify the temperature of that forecast. 
// Lastly, save the change back to the file.

import fs from "fs"

const forecast = JSON.parse(fs.readFileSync("./forecast_data.json", "utf-8"))

forecast.temperature++

fs.writeFileSync("./forecast_data.json", JSON.stringify(forecast), "utf-8")

console.log(forecast)