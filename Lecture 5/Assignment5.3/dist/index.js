"use strict";
// Assignment 5.3: Objective Recipes
// Create two interfaces: "Ingredient" and "Recipe". 
const sugar = {
    name: "sugar",
    amount: 1
};
const flour = {
    name: "flour",
    amount: 1
};
const water = {
    name: "water",
    amount: 1
};
const egg = {
    name: "egg",
    amount: 1
};
const dough = {
    name: "dough",
    ingredients: [sugar, flour, water, egg],
    servings: 1
};
console.log(dough);
