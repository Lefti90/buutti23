// Assignment 5.3: Objective Recipes
// Create two interfaces: "Ingredient" and "Recipe". 

// Ingredients should have two properties: name (string) and amount (number),
//  signifying the name and amount of a single ingredient used in a recipe. 
// Recipes should have three properties: name (string), ingredients (list of Ingredient objects) 
// and servings (number). 

// Create some ingredient objects and use them to create a recipe object.

interface Ingredient {
    name: string,
    amount: number
}

interface Recipe {
    name: string,
    ingredients: Ingredient[],
    servings: number
}

const sugar: Ingredient = {
    name: "sugar",
    amount: 1
}

const flour: Ingredient = {
    name: "flour",
    amount: 1
}

const water: Ingredient = {
    name: "water",
    amount: 1
}

const egg: Ingredient = {
    name: "egg",
    amount: 1
}

const dough: Recipe = {
    name: "dough",
    ingredients: [sugar, flour, water, egg],
    servings: 1
}

console.log(dough)
