"use strict";
// Assignment 5.3: Objective Recipes
// Create two interfaces: "Ingredient" and "Recipe". 
// Ingredients should have two properties: name (string) and amount (number),
//  signifying the name and amount of a single ingredient used in a recipe. 
// Recipes should have three properties: name (string), ingredients (list of Ingredient objects) 
// and servings (number). 
// Create some ingredient objects and use them to create a recipe object.
class Ingredient {
    constructor(name, amount) {
        this.name = name;
        this.amount = amount;
    }
}
class Recipe {
    constructor(name, ingredients, servings) {
        this.name = name;
        this.ingredients = ingredients;
        this.servings = servings;
    }
}
const sugar = new Ingredient("sugar", 1);
const flour = new Ingredient("flour", 1);
const water = new Ingredient("water", 1);
const egg = new Ingredient("egg", 1);
const dough = {
    name: "dough",
    ingredients: [sugar, flour, water, egg],
    servings: 1
};
console.log(dough);
