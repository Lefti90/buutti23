// Assignment 5.8: Read/Write File
// Implement a program that can read your given file, and print out all the information in it.

// Print out word for word, and every time the word you’re trying to print 
// is “joulu”, you replace that word with “kinkku”, and every time your printing 
// “lapsilla” you print “poroilla”.

// Finally, write the altered text to a new file.

import fs from "fs"

const laulu = fs.readFileSync("./joululaulu.txt", "utf-8").toLowerCase()

const sanat = laulu.split(" ")
let uudetSanat = ""

function swapWord(){
    for (let i = 0; i < sanat.length; i++) {
        if(sanat[i] == "joulu")
            sanat[i] = "kinkku"

        if(sanat[i] == "lapsilla")
            sanat[i] = "poroilla"
    }
    uudetSanat = sanat.join(" ")
}

swapWord()
console.log(sanat)
console.log(uudetSanat)

fs.writeFileSync("./uusilaulu.txt", uudetSanat, "utf-8")