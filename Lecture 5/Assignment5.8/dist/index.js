"use strict";
// Assignment 5.8: Read/Write File
// Implement a program that can read your given file, and print out all the information in it.
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Print out word for word, and every time the word you’re trying to print 
// is “joulu”, you replace that word with “kinkku”, and every time your printing 
// “lapsilla” you print “poroilla”.
// Finally, write the altered text to a new file.
const fs_1 = __importDefault(require("fs"));
const laulu = fs_1.default.readFileSync("./joululaulu.txt", "utf-8").toLowerCase();
const sanat = laulu.split(" ");
let uudetSanat = "";
function derp() {
    for (let i = 0; i < sanat.length; i++) {
        if (sanat[i] == "joulu")
            sanat[i] = "kinkku";
        if (sanat[i] == "lapsilla")
            sanat[i] = "poroilla";
    }
    uudetSanat = sanat.join(" ");
}
derp();
console.log(sanat);
console.log(uudetSanat);
//TODO: korvaa "moi"
fs_1.default.writeFileSync("./uusilaulu.txt", uudetSanat, "utf-8");
