import { useLoaderData } from 'react-router-dom'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function loader({params}: any) {
    return params.page
}

export default function Page() {
    const page = useLoaderData() as string
    return ( 
    <div className='Page'>
        This is page {page}!
    </div>
    )
}