import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Page, { loader as pageLoader } from "./Page"
import Contact, { loader as contactLoader } from './Contact'

const router = createBrowserRouter([
    {
        path: '/',
        element: <div>Hello Router!</div>,
    },
    {
      path: '/:page',
      element: <Page />,
      loader: pageLoader
    },
    {
      path: '/contact/:id',
      element: <Contact />,
      loader: contactLoader
    }
])

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>
)
