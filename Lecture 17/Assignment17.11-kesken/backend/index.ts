import express from 'express'
import router from './router'

const server = express()
server.use('/', express.static('./dist/client'))
server.use('/api/v1/', router)

server.get('*', (req, res) => {
    res.sendFile('index.html', { root: './dist/client' })
})

server.listen(3000, () => {
    console.log('Server listening port 3000')
})
