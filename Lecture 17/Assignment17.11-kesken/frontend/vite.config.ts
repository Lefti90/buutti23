import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    // Disable middleware mode
    middlewareMode: false
  },
  // Define aliases for routing
  // Ei toimi :/
  resolve: {
    alias: {
      '/': '/songs'
    }
  }
})