import { Link, Outlet } from 'react-router-dom'

export default function App(){
    return(
      <div className='App'>
        <nav>
          <Link className='link' to={'/songs/1'}>Song 1</Link>
          <Link className='link' to={'/songs/2'}>Song 2</Link>
          <Link className='link' to={'/songs/3'}>Song 3</Link>
          <Link className='link' to={'/songs/4'}>Song 3</Link>
          <Link className='link' to={'/songs/5'}>Song 3</Link>
        </nav>  
        <Outlet />
      </div>
    )
  }