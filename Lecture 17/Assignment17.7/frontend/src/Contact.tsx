import { useLoaderData } from 'react-router-dom'


interface Contact{
    id: number
    name: string
    phonenumber: string
    email: string
  }
  
  const contacts : Array<Contact> = [
    {
      "id": 1,
      "name": "John",
      "phonenumber": "0401234567",
      "email": "john@gmail.com"
    },
    {
      "id": 2,
      "name": "Jake",
      "phonenumber": "0402222222",
      "email": "jake@gmail.com"
    },
    {
      "id": 3,
      "name": "Julie",
      "phonenumber": "0403333333",
      "email": "julie@gmail.com"
    }
  ]

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function loader({params}: any) {
    console.log("PARAMSI:", params.id)

    const contactID = contacts.find(item => item.id == params.id)
    console.log("wat:", contactID)

    if(contactID === undefined){
      throw new Response('some message', {status: 404})
    }
    return params.id
}

export default function Contact() {
    const contact = useLoaderData() as string
    const contactID = Number(contact)-1
    return ( 
    <div className='Contact'>
        <p>{contacts[contactID].id}</p>
        <p>{contacts[contactID].name}</p>
        <p>{contacts[contactID].phonenumber}</p>
        <p>{contacts[contactID].email}</p>
    </div>
    )
}