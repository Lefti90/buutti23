import { useRouteError, isRouteErrorResponse, Link } from 'react-router-dom'

export default function ErrorPage() {
    const error = useRouteError()
  
    if (!isRouteErrorResponse(error)) {
      console.error("ERRORI:", error)
      return (
        <div className='ErrorPage'>
          <h1>An Unexpected error has happened!</h1>
          <p>This should not happen...</p>
        </div>
      )
    }

    if (error.status === 404) return (
      <div className='ErrorPage'>
          <h1>404 - Not Found</h1>
          <p>The requested element is not here</p>
          <Link to="/contact">Back to main</Link>
      </div>
    )
}
