import { useState } from "react";

function selectColor(setColor: (color: string) => void){
  const newColor = Math.floor(Math.random()*16777215).toString(16)
  setColor(newColor)
  return newColor
}

function App() {
  const [color, setColor] = useState("F1F2F3")
  console.log(color)

  const style = {
    backgroundColor: `#${color}`,
    padding: '0.5rem',
    fontFamily: 'sans-serif',
    fontSize: '1.5rem',
    boxShadow: `0 0 10px #${color}`
  }

  const handleClick = () => {
    selectColor(setColor)
  }

  return (
    <>
    <h1>{color}</h1>
    <div onClick={handleClick} style={style}>Click me to change color</div>
    </>
  )
}

export default App
