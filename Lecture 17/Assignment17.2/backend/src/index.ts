import express from 'express'
import router from './router'

const server = express()
server.use('/', express.static('./dist/client'))
server.use('/api/v1/', router)

server.listen(3000, () => {
    console.log('Server listening port 3000')
})
