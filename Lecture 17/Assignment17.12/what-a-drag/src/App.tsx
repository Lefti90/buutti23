import { useState } from 'react'

function App() {
  const winCenterX = (window.innerWidth-100)/2
  const winCenterY = (window.innerHeight-100)/2
  const [isDragging, setIsDragging] = useState(false)
  const [position, setPosition] = useState({ x: winCenterX, y: winCenterY})
  const [offset, setOffset] = useState({ x: 0, y: 0 })

  const style = {
    backgroundColor: `#76d3d6`,
    position: 'absolute',
    border: '5px solid',
    cursor: isDragging ? 'grabbing' : 'grab',
    width: 100,
    height: 100,
    padding: 10,
    left: position.x - offset.x,
    top: position.y - offset.y,
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleMouseDown = (event: any) => {
    setIsDragging(true)
    setOffset({
      x: event.nativeEvent.offsetX,
      y: event.nativeEvent.offsetY,
    })
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleMouseMove = (event: any) => {
    if (!isDragging) return
    setPosition({
      x: event.clientX,
      y: event.clientY,
    })
  }

  const handleMouseUp = () => {
    setIsDragging(false)
  }

  return (
    <>
    <div
      style={style}
      onMouseDown={handleMouseDown}
      onMouseMove={handleMouseMove}
      onMouseUp={handleMouseUp}>
    </div>
    </>
  )
}

export default App