import { useLoaderData } from "react-router-dom"

interface Song {
    id: number
    artist: string
    name: string
    lyrics: string
}

const songs: Array<Song> = [
    {
        "id": 1,
        "artist": "Pekka",
        "name": "Virsi 1",
        "lyrics": "Jeesus ristus jee"
    },
    {
        "id": 2,
        "artist": "Pate Mustajärvi",
        "name": "Ukkometso",
        "lyrics": "Kaljaa koneeseen ja ukkometso"
    },
    {
        "id": 3,
        "artist": "Leevi and the Leavings",
        "name": "Poika nimeltä Päivi",
        "lyrics": "Ei tiennyt onko Romeo vaikko Julia"
    },
    {
        "id": 4,
        "artist": "ASD",
        "name": "Virsi ASD4",
        "lyrics": "asdsad"
    },
    {
        "id": 5,
        "artist": "555",
        "name": "Virsi ASD5",
        "lyrics": "asdd5"
    },
]

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function loader({params}: any) {
    const song = songs.find(item => item.id == params.id)
    if(song === undefined){
      throw new Response('some message', {status: 404})
    }
    return song
}

export default function SongElement() {
    const song = useLoaderData() as Song
    return(
        <>
        <div className="SongElement">
            <h2>{song.name}</h2>
            <p>{song.artist}</p>
            <p>{song.lyrics}</p>        
        </div>
        </>
    )
}