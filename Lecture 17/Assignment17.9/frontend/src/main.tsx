import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import SongElement, {loader as songLoader} from './Songs'
import App from "./App"
import ErrorPage from './assets/ErrorPage'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        children:[
          {
            path: '/songs/:id',
            element: <SongElement />,
            errorElement: <ErrorPage />,
            loader: songLoader
          }
        ]
    }
])

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
