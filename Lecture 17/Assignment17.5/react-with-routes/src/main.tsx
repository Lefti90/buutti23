import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Contact, { loader as contactLoader } from './Contact'
import App from './App'
import './App.css'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        children: [
          {
            path: '/contact/:id',
            element: <Contact />,
            loader: contactLoader
          }
        ]
    },
])

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>
)
