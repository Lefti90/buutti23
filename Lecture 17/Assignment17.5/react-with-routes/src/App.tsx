import { Link, Outlet } from 'react-router-dom'

export default function App(){
    return(
      <div className='App'>
        <nav>
          <Link className='link' to={'/contact/1'}>Page1</Link>
          <Link className='link' to={'/contact/2'}>Page2</Link>
          <Link className='link' to={'/contact/3'}>Page3</Link>
        </nav>
  
        <Outlet />
      </div>
    )
  }