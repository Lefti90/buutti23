const inputString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"


//Hard coded:
const shortString = inputString.substring(0,21)
const lowString = shortString.toLowerCase()
const trimmedString = lowString.trim()

console.log(trimmedString)

//User input?:
const exampleString = process.argv[2];

const shortStringU = exampleString.substring(0,21)
const lowStringU = shortStringU.toLowerCase()
const trimmedStringU = lowStringU.trim()

if (exampleString) {
  console.log(`${trimmedStringU}`);
} else {
  console.log('Please provide an example string as a command-line argument.');
}

//Extra:
if(exampleString.length <= 20 && exampleString[0] !== " " && exampleString[exampleString.length-1] !== " " && exampleString[0] === exampleString[0].toLowerCase()){
    console.log("Nice string, very nice")
}else{
    console.log("Not nice :(")
}
