const playerCount = 4

//A:
if(playerCount === 4)
    console.log("Can play, yay!")

//B:
const isStressed = true
const hasIcecream = true
let markIsHappy = true

if(!isStressed || hasIcecream)
    markIsHappy = true

//C:
const temperature = 21
const raining = true
let beachDay = false

if(temperature > 20 && !raining)
    beachDay = true

//D:
let arinHappy = true
const isTuesday = true
const seenSusy = false
const seenDan = true

if(seenSusy === seenDan && isTuesday){
    arinHappy = false
    console.log(arinHappy)
}else{
    arinHappy = true
    console.log(arinHappy)
}