// Create variables for price, for price in euros, 
// and discount, for a discount percentage, and assign some values for those. 
// Calculate and console.log the original price, discount percentage, and the discounted price.

const priceInDollars = 100 // price in dollars?
let priceInEuros = 0
const discount = 20
let discountPercent = 0
let finalPrice = 0

//Calc values
priceInEuros = priceInDollars * 0.93
discountPercent = (discount / priceInEuros) * 100
finalPrice = priceInEuros - discount

// kannattaa alustaa muuttujat suoraan arvoihin ja käyttää constia sen sijaan, että laittaa muuttujalistan alkuun. Tulee vähemmän koodia ja koodia lukiessa ei on selvää heti mistä arvo tulee, kun muuttujaa ei määritellä kahdessa paikassa.


console.log(`Original price: ${priceInEuros}€. Discount: ${discountPercent}%. Final price: ${finalPrice}€`)
