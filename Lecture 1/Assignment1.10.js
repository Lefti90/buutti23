// Write a program that has four values: lastName, age, isDoctor, sender. 
// The name parameters should be strings, age a number and isDoctor a boolean.

const lastName = "Sugunimi"
const age = 33
const isDoctor = false
let titleText = ""
const sender = "Onnittelijat Ry:n hyvä väki"
let nextAge = age + 1
let nextAgeString = nextAge
let nextAgeFinal = ""


//Format isDoctor
if (isDoctor) {
    titleText = "Dr."
} else {
    titleText = "Mx."
}

//Format nextAge correctly
if (nextAge[nextAgeString.length] === 1) {
    nextAgeFinal = nextAge + "st"
} else if (nextAge[nextAgeString.length] === 2) {
    nextAgeFinal = nextAge + "nd"
} else if (nextAge[nextAgeString.length] === 3) {
    nextAgeFinal = nextAge + "rd"
} else {
    nextAgeFinal = nextAge + "th"
}

//Output
console.log(`Dear ${titleText} ${lastName}

Congratulations on your ${nextAgeFinal} birthday! Many happy returns!

Sincerely,
${sender}`)


