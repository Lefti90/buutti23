// Create a program that tells how many groups can be formed if the total 
// number of people and the group size is known.

const a = process.argv[2] || "Enter number of people as first parameter"
const b = process.argv[3] || "Enter the group size as second parameter"
const groups = Math.ceil(a / b)

console.log("Number of groups: ", groups)

// Laita vaan luentotehtävät myös mukaan (vrt esimerkit)