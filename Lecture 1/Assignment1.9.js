// Calculate how many seconds there are in a year. Use variables for days, hours, 
// seconds and seconds in a year. Print out the result with console.log.

const days = 365
const hours = days * 24
const minutes = hours * 60
const seconds = minutes * 60

console.log(seconds)